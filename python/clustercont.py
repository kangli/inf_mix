import numpy as np
from sklearn  import metrics
import sklearn.mixture.gaussian_mixture as GM
from sklearn.cluster import AgglomerativeClustering

def read(txt, n):
    labels = np.genfromtxt(txt)
    true_label = np.array([0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,2])
    
    result = [metrics.adjusted_mutual_info_score(true_label, labels[i]) for i in range(labels.shape[0])]
    
    result = np.array(result).reshape([n,4])

    return result.mean(0), (result == 1).mean(0)

def calc_cluster(filename):

    a = np.genfromtxt(filename)
    data = a.reshape([-1, 16, 16])

    res = np.zeros([100,5])
    acres = np.zeros([100,5])
    ac = AgglomerativeClustering(n_clusters=3, affinity="precomputed", linkage='average')

    true_label = np.array([0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,2])

    for i in range(100):
        # forward
        m_f = data[i]
        # backward
        m_b = m_f.T
        # combine
        m_c = np.concatenate([m_f, m_b], 1)
        # symmetric
        m_s = 0.5 * (m_f + m_b)
        # partial
        m_p = m_f[:,np.random.choice(16, 6, replace=False)]

        gm = GM.GaussianMixture(
            n_components = 3,
            covariance_type = "full",
            n_init = 5,
            tol = 1e-5
        )
        gm.fit(m_f)
        pred1 = gm.predict(m_f)
        ac1 = ac.fit(m_f).labels_

        gm.fit(m_b)
        pred2 = gm.predict(m_b)
        ac2 = ac.fit(m_b).labels_

        gm.fit(m_c)
        pred3 = gm.predict(m_c)
        ac3 = ac.fit(m_c).labels_

        gm.fit(m_s)
        pred4 = gm.predict(m_s)
        ac4 = ac.fit(m_s).labels_

        gm2 = GM.GaussianMixture(
            n_components = 3,
            covariance_type = "full",
            n_init = 5,
            tol = 1e-5
        )
        gm2.fit(m_p)
        pred5 = gm2.predict(m_p)
        ac5 = pred5
        #ac5 = ac.fit(m_p).labels_

        res[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, pred1),
             metrics.adjusted_rand_score(true_label, pred2),
             metrics.adjusted_rand_score(true_label, pred3),
             metrics.adjusted_rand_score(true_label, pred4),
             metrics.adjusted_rand_score(true_label, pred5)]
        )
        acres[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, ac1),
             metrics.adjusted_rand_score(true_label, ac2),
             metrics.adjusted_rand_score(true_label, ac3),
             metrics.adjusted_rand_score(true_label, ac4),
             metrics.adjusted_rand_score(true_label, ac5)]
        )

    return res.mean(0), (res==1).mean(0), acres.mean(0), (acres==1).mean(0), ["forward", "backward", "combined", "symmetric", "forward_partial"]
    



