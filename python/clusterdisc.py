#%%
import ctypes
import numpy as np
from numpy.ctypeslib import ndpointer
import sklearn.mixture.gaussian_mixture as GM
from sklearn.cluster import AgglomerativeClustering

import matplotlib.pyplot as plt

from sklearn import metrics

lib = ctypes.cdll.LoadLibrary("../build/libdisclib.so")



#%%
lib.stimulus.restype = ctypes.c_void_p

lib.stim_set_stim.argtypes = [ctypes.c_void_p, ndpointer(ctypes.c_int, flags="C_CONTIGUOUS")]

lib.point_process.restype = ctypes.c_void_p
lib.point_process.argtypes = [ctypes.c_void_p]

lib.pp_generate_data.argtypes = [ctypes.c_void_p, ndpointer(ctypes.c_int, flags="C_CONTIGUOUS")]

lib.stim_calc.argtypes = [ctypes.c_void_p, 
                          ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                          ctypes.c_int,
                          ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]

lib.vec2arr.argtypes = [ctypes.c_void_p, ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                        ctypes.c_int]

lib.arr2vec.argtypes = [ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                        ctypes.c_void_p, ctypes.c_int]

lib.new_vec.restype = ctypes.c_void_p

lib.run.argtypes = [ctypes.c_int,
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS")]

lib.check_inits.argtypes = [
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS")]

lib.test2.argtypes = [ ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_double, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ndpointer(ctypes.c_int, flags="C_CONTIGUOUS"),
                    ctypes.c_bool]



def plot_stim(stim, h = True, color="red"):
    pstim = lib.stimulus()
    lib.stim_set_stim(pstim, stim)
    ts = np.arange(0, 0.5, 0.001)
    res = np.zeros(ts.size)

    lib.stim_calc(pstim, ts, ts.size, res)

    plt.plot(ts, res, color=color)
    if h:
        plt.draw()

def plot_ss(ss, h = True):
    ts = np.arange(ss.size)
    plt.scatter((ts[ss == 1]+1)/1000, ss[ss == 1], marker="|")
    if h:
        plt.show()


def plot_stims(s1, s2, s3, a1, a2, a3, b1, b2, b3, c1, c2, c3,
               a4, a5, a6, b4, b5, b6, c4, c5, c6):
    plt.subplots(nrows=1, ncols=2)
    plt.subplot(1, 2, 1)
    plot_stim(s1, False, color="red")
    plot_stim(s2, False, color="red")
    plot_stim(s3, False, color="red")
    plot_stim(a1, False, color="blue")
    plot_stim(a2, False, color="blue")
    plot_stim(a3, False, color="blue")
    plot_stim(b1, False, color="green")
    plot_stim(b2, False, color="green")
    plot_stim(b3, False, color="green")
    plot_stim(c1, False, color="purple")
    plot_stim(c2, False, color="purple")
    plot_stim(c3, False, color="purple")
    plt.subplot(1, 2, 2)
    plot_stim(s1, False, color="red")
    plot_stim(s2, False, color="red")
    plot_stim(s3, False, color="red")
    plot_stim(a4, False, color="blue")
    plot_stim(a5, False, color="blue")
    plot_stim(a6, False, color="blue")
    plot_stim(b4, False, color="green")
    plot_stim(b5, False, color="green")
    plot_stim(b6, False, color="green")
    plot_stim(c4, False, color="purple")
    plot_stim(c5, False, color="purple")
    plot_stim(c6, False, color="purple")
    plt.show()

#%%

#%%

#stimseq = np.array([2,1,0,4,3,2,1,3,5], dtype=np.int32)
#
#stim = lib.stimulus()
#lib.stim_set_stim(stim, stimseq)
#
#sslen = 500
#pp = lib.point_process(stim, sslen)
#
#data = np.zeros(sslen, dtype=np.int32)
#lib.pp_generate_data(pp, data)
#
#plot_stim(stimseq, False)
#plot_ss(data)
#


#stim = np.array([0,1,1,1,2,4,2,0,5], dtype=np.int32)
#mllk = np.zeros(1)
#min_stim = np.zeros(9, dtype=np.int32)
#min_mllk = np.zeros(1)

def test():
    s1 = np.random.choice(np.arange(6, dtype=np.int32), 16)
    s2 = np.random.choice(np.arange(6, dtype=np.int32), 16)
    s3 = np.random.choice(np.arange(6, dtype=np.int32), 16)
    
    a1 = np.zeros(16, dtype=np.int32)
    a2 = np.zeros(16, dtype=np.int32)
    a3 = np.zeros(16, dtype=np.int32)
    b1 = np.zeros(16, dtype=np.int32)
    b2 = np.zeros(16, dtype=np.int32)
    b3 = np.zeros(16, dtype=np.int32)
    c1 = np.zeros(16, dtype=np.int32)
    c2 = np.zeros(16, dtype=np.int32)
    c3 = np.zeros(16, dtype=np.int32)

    a4 = np.zeros(16, dtype=np.int32)
    a5 = np.zeros(16, dtype=np.int32)
    a6 = np.zeros(16, dtype=np.int32)
    b4 = np.zeros(16, dtype=np.int32)
    b5 = np.zeros(16, dtype=np.int32)
    b6 = np.zeros(16, dtype=np.int32)
    c4 = np.zeros(16, dtype=np.int32)
    c5 = np.zeros(16, dtype=np.int32)
    c6 = np.zeros(16, dtype=np.int32)
    
    lib.check_inits(s1, s2, s3, a1, a2, a3, b1, b2, b3, c1, c2, c3,
                                a4, a5, a6, b4, b5, b6, c4, c5, c6)
    
    plot_stims(s1, s2, s3, a1, a2, a3, b1, b2, b3, c1, c2, c3, 
                           a4, a5, a6, b4, b5, b6, c4, c5, c6)

num = 100            # num of repetitions 
k = 4               # number of clusters
rep = 1             # number of em trials
num_stim = 16       # number of stimuli
ds = 5

def test2():
    data_size = 10*k      # number of observations
    
    mllks = np.zeros(num*4, dtype=np.float64)
    init_time = np.zeros(num*4, dtype=np.float64)
    em_time = np.zeros(num*4, dtype=np.float64)
    labels = np.zeros(data_size*4*num, dtype=np.int32)
    stims = np.zeros(num_stim*k*4*num, dtype=np.int32)
    true_labels = np.zeros(data_size*num, dtype=np.int32)

    lib.test2(k, num, rep, data_size, mllks, init_time, em_time, labels, stims, true_labels, False)
    
    mllks = mllks.reshape([num, 4])
    init_time = init_time.reshape([num, 4])
    em_time = em_time.reshape([num, 4])
    labels = labels.reshape([num, 4, data_size])
    true_labels = true_labels.reshape([num, data_size])
    stims = stims.reshape([num, 4, k, num_stim])

    cluster_scores = np.zeros([num, 4])
    for i in range(num):
        #f = map(lambda pred: metrics.adjusted_rand_score(true_labels[i,], pred),
        f = map(lambda pred: metrics.adjusted_mutual_info_score(true_labels[i,], pred),
                labels[i,:,:])
        cluster_scores[i,:] = np.fromiter(f, dtype=np.float64)

    return [mllks, init_time, em_time, labels, stims, true_labels, cluster_scores]

def readinput(prefix=""):
    #data_size = k*(k-2)*(k-2)*10      # number of observations
    data_size = ds*k

    suffix = str(k)+"_"+str(rep)+"_"+str(ds)
    mllks = np.fromfile(prefix+"mllk_"+suffix+".txt", sep=" ")
    init_time = np.fromfile(prefix+"init_time_"+suffix+".txt", sep=" ")
    em_time = np.fromfile(prefix+"em_time_"+suffix+".txt", sep=" ")
    labels = np.fromfile(prefix+"labels_"+suffix+".txt", sep=" ")
    true_labels = np.fromfile(prefix+"true_labels_"+suffix+".txt", sep=" ")
    stims = np.fromfile(prefix+"stims_"+suffix+".txt", sep=" ")

    mllks = mllks.reshape([num, 4])
    init_time = init_time.reshape([num, 4])
    em_time = em_time.reshape([num, 4])
    labels = labels.reshape([num, 4, data_size])
    true_labels = true_labels.reshape([num, data_size])
    stims = stims.reshape([num, 4, k, num_stim])

    cluster_scores = np.zeros([num, 4])
    for i in range(num):
        f = map(lambda pred: metrics.adjusted_rand_score(true_labels[i,], pred),
        #f = map(lambda pred: metrics.homogeneity_completeness_v_measure(true_labels[i,], pred)[2],
                labels[i,:,:])
        cluster_scores[i,:] = np.fromiter(f, dtype=np.float64)

    return [mllks, init_time, em_time, labels, stims, true_labels, cluster_scores]

def readinputldmatrix(prefix=""):
    #data_size = k*(k-2)*(k-2)*10      # number of observations
    data_size = ds*k

    suffix = str(k)+"_"+str(rep)+"_"+str(ds)
    labels = np.fromfile(prefix+"labels_"+suffix+".txt", sep=" ")
    true_labels = np.fromfile(prefix+"true_labels_"+suffix+".txt", sep=" ")

    labels = labels.reshape([num, 4, data_size])
    true_labels = true_labels.reshape([num, data_size])

    cluster_scores = np.zeros([num, 4])
    for i in range(num):
        f = map(lambda pred: metrics.adjusted_rand_score(true_labels[i,], pred),
        #f = map(lambda pred: metrics.homogeneity_completeness_v_measure(true_labels[i,], pred)[2],
                labels[i,:,:])
        cluster_scores[i,:] = np.fromiter(f, dtype=np.float64)

    return np.array(cluster_scores)

def calc_cluster(filename, filelabels):
    true_labels = np.genfromtxt(filelabels)
    data = np.genfromtxt(filename).reshape([-1,40,40])

    res = np.zeros([100,5])
    acres = np.zeros([100,5])
    ac = AgglomerativeClustering(n_clusters=4, affinity="precomputed", linkage='average')

    for i in range(100):
        true_label = true_labels[i]

        # backward
        m_b = data[i]
        # forwrd
        m_f = m_b.T
        # combine
        m_c = np.concatenate([m_b, m_f], 1)
        # symmetric
        m_s = 0.5 * (m_b + m_f)
        # partial
        m_p = m_f[:,np.random.choice(16, 6, replace=False)]

        gm = GM.GaussianMixture(
            n_components = 4,
            covariance_type = "diag",
            n_init = 5,
            tol = 1e-5
        )
        gm.fit(m_f)
        pred1 = gm.predict(m_f)
        ac1 = ac.fit(m_f).labels_

        gm.fit(m_b)
        pred2 = gm.predict(m_b)
        ac2 = ac.fit(m_b).labels_

        gm.fit(m_c)
        pred3 = gm.predict(m_c)
        ac3 = ac.fit(m_c).labels_

        gm.fit(m_s)
        pred4 = gm.predict(m_s)
        ac4 = ac.fit(m_s).labels_

        gm2 = GM.GaussianMixture(
            n_components = 4,
            covariance_type = "diag",
            n_init = 5,
            tol = 1e-5
        )
        gm2.fit(m_p)
        pred5 = gm2.predict(m_p)
        ac5 = pred5
        #ac5 = ac.fit(m_p).labels_

        res[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, pred1),
             metrics.adjusted_rand_score(true_label, pred2),
             metrics.adjusted_rand_score(true_label, pred3),
             metrics.adjusted_rand_score(true_label, pred4),
             metrics.adjusted_rand_score(true_label, pred5)]
        )
        acres[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, ac1),
             metrics.adjusted_rand_score(true_label, ac2),
             metrics.adjusted_rand_score(true_label, ac3),
             metrics.adjusted_rand_score(true_label, ac4),
             metrics.adjusted_rand_score(true_label, ac5)]
        )

    return res.mean(0), (res==1).mean(0), acres.mean(0), (acres==1).mean(0), ["forward", "backward", "combined", "symmetric", "forward_partial"]
