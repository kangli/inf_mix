from __future__ import absolute_import, division, print_function

import numpy as np
import tensorflow as tf
import math

from scipy.stats import norm
from scipy.stats import poisson

from collections import namedtuple

HUGE_VAL = 1e100
fit_file = "./modelsave.ckpt"
simul_num_each_sec_mean = 200
simul_num_each_sec_sd = 20

base_lr = 0.1
base_nsteps = 500

stim_l = 5.0
stim_h = 10.0

def stim_unif_rand(a = stim_l, b = stim_h):
    return np.random.uniform(a, b, 3)


# log sum exp
def lse(ll):
    m = tf.reduce_max(ll, 1, keepdims=True)
    return tf.reduce_mean(m + tf.log(tf.reduce_sum(tf.exp(ll - m), 1, keepdims=True)))

def nn_model(model):
    # layers
    pos = tf.placeholder(tf.float32, [None, 3])

    x, y, z = tf.split(pos, [1,1,1], 1)

    if model == "train":
        input_stims = tf.placeholder(tf.float32, [None, 3])
    elif model == "infer":
        input_stims = tf.get_variable('stims', [1, 3], 
                initializer = tf.random_uniform_initializer(0.0, 20.0))
    elif model == "mllk":
        input_stims = tf.placeholder(tf.float32, [1, 3])

    learning_rate = tf.placeholder(tf.float32, shape=[])
    
    # dense layers
    #h1 = tf.layers.dense(inputs = input_stims, units = 32, activation = tf.nn.relu)
    #h2 = tf.layers.dense(inputs = h1, units = 32, activation = tf.nn.relu)
    #xsd = tf.layers.dense(inputs = h2, units = 1, activation = tf.nn.relu) + 1e-3

    #h3 = tf.layers.dense(inputs = x, units = 32, activation = tf.nn.relu)
    #h4 = tf.layers.dense(inputs = h1, units = 32, activation = tf.nn.relu)
    #h5 = tf.layers.dense(inputs = tf.concat([h3, h4], 1), units = 32, activation = tf.nn.relu)
    #ymean = tf.layers.dense(inputs = h5, units = 1, activation = tf.nn.relu)

    #h6 = tf.layers.dense(inputs = y, units = 32, activation = tf.nn.relu)
    #h7 = tf.layers.dense(inputs = h1, units = 32, activation = tf.nn.relu)
    #h8 = tf.layers.dense(inputs = tf.concat([h6, h7], 1), units = 32, activation = tf.nn.relu)
    #zmean = tf.layers.dense(inputs = h8, units = 1, activation = tf.nn.relu)

    w = tf.get_variable("w", dtype=tf.float32,
        initializer=tf.constant([[0.1, 0.8, 0.1], 
                                 [0.7, 0.3, 0.0], 
                                 [0.0, 0.4, 0.6]] 
    ))
    r = tf.matmul(input_stims, tf.transpose(w))
    r0, r1, r2 = tf.split(r, [1,1,1], 1)
    s0, s1, s2 = tf.split(input_stims, [1,1,1], 1)

    ymean = r1/r0*tf.sqrt(tf.abs(r0*r0-x*x))

    zmean = tf.pow(r2/r1, 1-tf.sign(y-ymean)) * tf.pow(r2/r0, 1+tf.sign(y-ymean))*\
                    tf.sqrt(tf.abs(r0*r0-(y-r0)*x))
    xsd = r0*0.3+0.1
    ysd = tf.abs(tf.sin(x))*tf.sqrt(s0*0.1)+0.1
    zsd = tf.abs(tf.cos(y))*tf.sqrt(s2*0.1)+0.1
    
    # PDF
    # each component is a multidimensional normal
    # distn is the nth component
    distx = tf.distributions.Normal(0.0, xsd)
    disty = tf.distributions.Normal(ymean, ysd)
    distz = tf.distributions.Normal(zmean, zsd)
    
    px = distx.log_prob(x)
    py = disty.log_prob(y)
    pz = distz.log_prob(z)

    loss = - tf.reduce_mean(px) - tf.reduce_mean(py) - tf.reduce_mean(pz)

    ModelRet = namedtuple('ModelRet', 'loss, input, output, params, lr')

    return ModelRet(loss, input_stims, pos, [xsd, ymean, ysd, zmean, zsd], learning_rate)


def nn_session(model, restore_file):
    gr = tf.Graph()

    with gr.as_default():
        mdr = nn_model(model)

        optimizer = None
        train_op = None
        opt_hyper = None
        if model == "train":
            optimizer = tf.train.GradientDescentOptimizer(mdr.lr)
            train_op = optimizer.minimize(mdr.loss)
        elif model == "infer":
            beta1 = tf.get_variable("beta1", dtype=tf.float32,
                initializer=tf.constant(0.9))
            beta2 = tf.get_variable("beta2", dtype=tf.float32,
                initializer=tf.constant(0.999))
            optimizer = tf.train.AdamOptimizer(mdr.lr, beta1=beta1, beta2=beta2)
            all_variables = tf.trainable_variables()
            train_op = optimizer.minimize(mdr.loss, 
                    var_list = [all_variables[0]])
            opt_hyper = [beta1, beta2]

        sess = tf.Session()
        sess.run(tf.global_variables_initializer())

        if model != "infer":
            saver = tf.train.Saver()
        else:
            # restore and fix the other parameters
            all_variables = tf.trainable_variables()
            saver = tf.train.Saver(all_variables[1:])

        if restore_file is not None:
            saver.restore(sess, restore_file)


    SessRet = namedtuple("SessRet", 'mdr, graph, sess, saver, train_op, opt_hyper')

    return SessRet(mdr, gr, sess, saver, train_op, opt_hyper)



# pos_data is a nx3 np array
def mllk_all(sr, stim_data, pos_data, ce = True):
    mllk = sr.sess.run(sr.mdr.loss, {sr.mdr.input: [stim_data],
        sr.mdr.output: pos_data, sr.mdr.lr: base_lr})
    if not ce:
        mllk *= pos_data.shape[0]
    #print ("mllk: ", mllk)
    return mllk

# pos_data is a python list of nx3 np arrays
def mllk(sr, stim_data, pos_data, ce = True):
    mllks = np.zeros(len(pos_data))
    for i in range(len(pos_data)):
        mllks[i] = sr.sess.run(sr.mdr.loss, {sr.mdr.input: [stim_data],
            sr.mdr.output: pos_data[i], sr.mdr.lr: base_lr})
        if not ce:
            mllks[i] *= pos_data[i].shape[0]
    #print ("mllks: ", mllks)
    return mllks


def train(sr, input_data, output_data, save_file):
    batch_size = 1000
    
    for i in range(20000):
        sz = len(input_data)
        idx = np.random.choice(sz, batch_size)
        input_batch = [input_data[j] for j in idx]
        output_batch = [output_data[j] for j in idx]
        sr.sess.run(sr.train_op, {sr.mdr.input: input_batch, 
            sr.mdr.output: output_batch, sr.mdr.lr: base_lr})
    
        if i % 100 == 0:
            print(sr.sess.run(sr.mdr.loss, {sr.mdr.input: input_data, 
                sr.mdr.output: output_data, sr.mdr.lr: base_lr}))
        if i % 1000 == 0:
            sr.saver.save(sr.sess, save_file)


def infer(sr, pos_data, stim_init, lr = base_lr, nsteps = base_nsteps):
    # the first element is the stimulus to infer
    with sr.graph.as_default():
        all_variables = tf.trainable_variables()
        #sr.sess.run(all_variables[0].initializer)
        #sr.sess.run([sr.opt_hyper[i].initializer for i in range(len(sr.opt_hyper))])
        sr.sess.run(tf.global_variables_initializer())
    
        if stim_init is not None:
            sr.sess.run(all_variables[0].assign(np.array([stim_init])))
    
    for i in range(nsteps):
        sr.sess.run(sr.train_op, {sr.mdr.output: pos_data, sr.mdr.lr: lr})
        tmp = sr.sess.run(sr.mdr.loss, {sr.mdr.output: pos_data, sr.mdr.lr: lr})
        if math.isnan(tmp):
            print(tmp)
            return None
    print(tmp)
    
    v =  sr.sess.run(sr.mdr.input)
    return v, tmp*pos_data.shape[0]


def infer_n(sr_infer, sr_mllk, pos_data, n, stim_init):
    if stim_init is None:
        loss_min = HUGE_VAL
        stim_min = stim_unif_rand()
    else:
        loss_min = mllk_all(sr_mllk, stim_init, pos_data, False)
        stim_min = stim_init
        if math.isnan(loss_min):
            loss_min = HUGE_VAL
            stim_min = stim_unif_rand()

    for i in range(n):
        init = None
        if i == 0:
            init = stim_init
        infer_res = None
        steps = 0
        while infer_res is None:
            if steps < 10:
                infer_res = infer(sr_infer, pos_data, init, base_lr/(steps+1), base_nsteps+100*steps)
            else:
                infer_res = infer(sr_infer, pos_data, None, base_lr/np.power(10,steps-8), base_nsteps+100*steps)
            if steps > 25:
                loss = HUGE_VAL
                break
            steps += 1
        if infer_res is not None:
            loss = infer_res[1]
        if loss < loss_min:
            stim_min = infer_res[0]
    return stim_min




def simulate(sr, stim, size):
    alldata = []
    for i in range(size):
        nr = np.rint(np.random.normal(simul_num_each_sec_mean, 
                simul_num_each_sec_sd, size=size)[0]).astype(int)
        data = np.zeros([nr, 3])
        stimdata = np.repeat([stim], nr, axis=0)
        res = sr.sess.run(sr.mdr.params, 
                {sr.mdr.input: stimdata, sr.mdr.output: data, sr.mdr.lr: base_lr}
        )
    
        x = np.random.normal(0.0, res[0].flatten(), size=nr)
        data[:,0] = x
    
        res = sr.sess.run(sr.mdr.params, 
                {sr.mdr.input: stimdata, sr.mdr.output: data, sr.mdr.lr: base_lr}
        )
    
        y = np.random.normal(res[1].flatten(), res[2].flatten(), size=nr)
        data[:,1] = y
    
        res = sr.sess.run(sr.mdr.params, 
                {sr.mdr.input: stimdata, sr.mdr.output: data, sr.mdr.lr: base_lr}
        )
    
        z = np.random.normal(res[3].flatten(), res[4].flatten(), size=nr)
        data[:,2] = z

        alldata.append(data)
    return alldata

def simulate_n(sr, n, sizes):
    stims = np.zeros([n, 3])
    data_raw = []
    label_raw = []
    for i in range(n):
        stims[i,:] = stim_unif_rand()
        data = simulate(sr, stims[i,:], sizes[i])
        data_raw += data
        label_raw.append(np.zeros(sizes[i])+i)
    return data_raw, np.concatenate(label_raw), stims


