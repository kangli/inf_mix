from neural import *

import sklearn.mixture.gaussian_mixture as GM
from sklearn.cluster import AgglomerativeClustering
from sklearn import metrics

sr_train = nn_session("train", None)
#sr_train_cont = nn_session("train", fit_file)
sr_infer = nn_session("infer", None)
sr_mllk = nn_session("mllk", None)

infer_rep = 3

num_cluster = 4
size_cluster = [100, 100, 100, 100]
ldci_method = "forward_partial"

em_rep = 3

#def direct_em(data, k, true_label):
#    gm = GM.GaussianMixture(
#        n_components = k,
#        covariance_type = "spherical",
#        n_init = 5
#    )
#    gm.fit(data)
#    pred = gm.predict(data)
#    return pred, metrics.adjusted_rand_score(true_label, pred)

# a column of m is ld from all data to a inferred stim
def ld_matrix(data):
    sz = len(data)
    m = np.zeros([sz, sz])
    stims = np.zeros([sz, 3])
    for i in range(sz):
        print("###### ld matrix ####### ", i)
        stims[i,:] = infer_n(sr_infer, sr_mllk, data[i], infer_rep, None)
    for i in range(sz):
        m[:, i] = mllk(sr_mllk, stims[i,:], data)
    return m, stims

def ld_matrix_partial(data, subdata):
    szr = len(data)
    szc = len(subdata)
    m = np.zeros([szr, szc])
    stims = np.zeros([szc, 3])
    for i in range(szc):
        print("###### ld matrix ####### ", i)
        stims[i,:] = infer_n(sr_infer, sr_mllk, subdata[i], infer_rep, None)
    for i in range(szc):
        m[:, i] = mllk(sr_mllk, stims[i,:], data)
    return m, stims

def ld_matrix_nth(data, nth, m):
    sz = len(data)
    if m is None:
        m = np.zeros([sz, sz])
    stim = infer_n(sr_infer, sr_mllk, data[nth], infer_rep, None).reshape([3])
    m[:, nth] = mllk(sr_mllk, stim, data)
    return m, stim


def random_init(data, k):
    inits = np.zeros([k, 3])
    for i in range(k):
        print("###### rand init ####### ", i)
        index = np.random.choice(len(data), 1)[0]
        inits[i, :] = infer_n(sr_infer, sr_mllk, data[index], infer_rep, None)
    return inits

def kmpp(data, k):
    sz = len(data)
    ind = np.random.choice(sz, 1)[0]
    inds = np.array([ind])
    inits = np.zeros([k, 3])
    v = np.zeros(sz)
    m, stim = ld_matrix_nth(data, ind, None)
    inits[0,:] = stim

    v[:] = m[:,ind]
    v -= v.min()
    v[ind] = 0
    v /= v.sum()

    for i in range(1, k):
        print("###### kmpp init ####### ", i)
        ind = np.random.choice(sz, 1, p=v)[0]
        inds = np.append(inds, ind)
        m, stim = ld_matrix_nth(data, ind, m)
        v[:] = m[:, inds].min(1)
        v -= v.min()
        v[inds] = 0
        v /= v.sum()
        inits[i,:] = stim

    print(inds)
    return inits

def ldci(data, k, multi, method = "forward_partial"):
    sz = len(data)
    szm = k * k * multi
    if szm > sz:
        szm = sz
    ids = np.random.choice(sz, szm, replace=False)
    subdata = [data[i] for i in ids]

    em_cov = "spherical"
    if method == "both":
        m, stims = ld_matrix(subdata)
        m = np.concatenate([m, m.T], 1)
    elif method == "forward":
        # each row is the ld from each data point to a infered stim
        m, stims = ld_matrix(subdata)
    elif method == "reverse":
        m, stims = ld_matrix(subdata)
        m = m.T
    elif method == "forward_partial":
        m, stims = ld_matrix_partial(data, subdata)
        em_cov = "diag"

    gm = GM.GaussianMixture(
        n_components = k,
        covariance_type = em_cov,
        n_init = 5
    )
    gm.fit(m)
    pred = gm.predict(m)

    inits = np.zeros([k,3])
    infer_data = subdata
    if method == "forward_partial":
        infer_data = data
    for i in range(k):
        print("###### ldci init ####### ", i)
        c_data = [infer_data[j] for j in range(len(infer_data)) if (pred==i)[j]]
        if len(c_data) == 0:
            inits[i,:] = stim_unif_rand()
        else:
            init_stim = infer_n(sr_infer, sr_mllk, c_data[0], infer_rep, None).reshape([3])
            inits[i,:] = infer_n(sr_infer, sr_mllk, np.concatenate(c_data),
                infer_rep, init_stim)

    return inits, pred, m

def ldc(data, k, true_label, multi, method="forward_partial"):
    inits, pred, m = ldci(data, k, multi, method)
    return inits, pred, m, metrics.adjusted_rand_score(true_label, pred)


def ldctest(n):
    res = np.zeros([n,5])
    acres = np.zeros([n,5])
    ac = AgglomerativeClustering(n_clusters=4, affinity="precomputed", linkage='average')
    for i in range(n):
        data, true_label, _ = simulate_n(sr_train, num_cluster, size_cluster)
        k = num_cluster
        m, _ = ld_matrix(data)
        mm = np.concatenate([m, m.T], 1)

        ms = m + m.T

        gm = GM.GaussianMixture(
            n_components = k,
            covariance_type = "spherical",
            n_init = 5
        )
        gm.fit(mm)
        pred1 = gm.predict(mm)
        ac1 = ac.fit(mm).labels_
        gm.fit(m)
        pred2 = gm.predict(m)
        ac2 = ac.fit(m).labels_
        gm.fit(m.T)
        pred3 = gm.predict(m.T)
        ac3 = ac.fit(mt).labels_
        gm.fit(ms)
        pred5 = gm.predict(ms)
        ac5 = ac.fit(ms).labels_


        #ward = AgglomerativeClustering(n_clusters=4, linkage='ward').fit(m)

        m_p = m[:, np.random.choice(len(data), k*k, replace=False)]
        gm2 = GM.GaussianMixture(
            n_components = k,
            covariance_type = "diag",
            n_init = 5
        )
        gm2.fit(m_p)
        pred4 = gm2.predict(m_p)
        ac4 = pred4
        #ac4 = ac.fit(m_p).labels_
        
        res[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, pred1),
             metrics.adjusted_rand_score(true_label, pred2),
             metrics.adjusted_rand_score(true_label, pred3),
             metrics.adjusted_rand_score(true_label, pred5),
             metrics.adjusted_rand_score(true_label, pred4)]
        )
        acres[i,:] = np.array(
            [metrics.adjusted_rand_score(true_label, ac1),
             metrics.adjusted_rand_score(true_label, ac2),
             metrics.adjusted_rand_score(true_label, ac3),
             metrics.adjusted_rand_score(true_label, ac5),
             metrics.adjusted_rand_score(true_label, ac4)]
        )
    return res, acres, ["both", "forward", "reverse", "sym", "forward_partial"]


def optimal_bayes(data, k, true_label, true_stims):
    print("###### optimal bayes #######")
    mllks = np.zeros([len(data), k])
    for i in range(k):
        mllks[:,i] = mllk(sr_mllk, true_stims[i], data, False)
    indexes = np.argmin(mllks, axis = 1)
    loss = np.min(mllks, axis = 1).sum()

    return 0, loss, metrics.adjusted_rand_score(true_label, indexes)


def neural_hem(data, k, true_label, init_method, inits = None):
    print("###### hem init ####### ")
    if inits is None:
        if init_method == "rand":
            inits = random_init(data, k)
        elif init_method == "kmpp":
            inits = kmpp(data, k)
        elif init_method == "ldci":
            inits, _, _, _  = ldc(data, k, true_label, 2)

    loss_old = HUGE_VAL
    rtol = 1e-4

    for emloop in range(30):
        print("###### hem ####### loop ", emloop)
        # cluster
        mllks = np.zeros([len(data), k])
        for i in range(k):
            mllks[:,i] = mllk(sr_mllk, inits[i], data, False)
        indexes = np.argmin(mllks, axis = 1)
        loss = np.min(mllks, axis = 1).sum()

        if np.abs(loss - loss_old) / np.abs(rtol + loss) > rtol:
            loss_old = loss
        else:
            break
    
        # reestimate
        for i in range(k):
            print("###### hem reestimate ####### ", i, " ", np.sum(indexes==i))
            if np.sum(indexes == i) > 0:
                inits[i,:] = infer_n(sr_infer, sr_mllk, 
                    np.concatenate(
                        [data[j] for j in range(len(data)) if (indexes==i)[j]]
                    ),
                    infer_rep, inits[i,:])

    return emloop, loss, metrics.adjusted_rand_score(true_label, indexes)


def run(n):
    rs = np.zeros([n*em_rep, 5, 3])
    for i in range(n):
        pos, label, true_stims = simulate_n(sr_train, num_cluster, size_cluster)
    
        for j in range(em_rep):
            r1=neural_hem(pos, num_cluster, label, "rand")
            r2=neural_hem(pos, num_cluster, label, "kmpp")
            inits, _, _, score=ldc(pos, num_cluster, label, 2)
            r4=neural_hem(pos, num_cluster, label, "ldci", inits)
            r5=optimal_bayes(pos, num_cluster, label, true_stims)

            rs[i*em_rep+j,:,:] = np.array([r1, r2, [0, 0, score], r4, r5])
    return rs

def plotdata(n, m):
    import matplotlib.pyplot as plt   
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    for i in range(n):                                  
        a=np.concatenate(simulate_n(sr_train, 1, [m])[0])
        ax.scatter(a[:,0], a[:,1], a[:,2])
    
    fig.show()


def processresults():

    hemrs = np.zeros([5,3,100,2])

    rs = []

    for i in range(10):
        rs.append(np.load("rsem"+str(i)+".npy").reshape([10,3,5,3]))

    rs = np.concatenate(rs)

    for j in range(5):
        for i in range(1,4):
            minid = rs[:,0:i,j,1].argmin(axis=1)
            hemrs[j,i-1,:,:] = rs[np.arange(100),minid,j,1:3]

    res = np.concatenate([hemrs.mean(2),(hemrs[:,:,:,[1]]==1).mean(2)],2)

    loop = rs[:,:,:,0].reshape([-1,5]).mean(0)

    return res,loop



#


#train(sr_train, d1, d2, fit_file)

rs = run(50)

#np.save("rs", np.array(rs))

size_cluster = [10, 10, 10, 10]
ldrs = ldctest(100)
ldci_method = ldrs[1][np.argmax(ldrs[0].mean(0))]
#
#np.save("ldci", ldrs[0])
#np.save("ldci_method", np.array(ldci_method))

#size_cluster = [100, 100, 100, 100]
#rs = run(2)
#
#np.save("rs", rs)

#tmp = neural_hem(rates, 4)

#stims, rates2 = training_data_stim(stim, 10)

#stim_infer = infer(fit_file, rates).reshape([3,3])

# em





