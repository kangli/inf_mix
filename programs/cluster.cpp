#include <mass/LIFModels.h>
#include <chrono>
#include <ctime>
#include <fstream>
#include <map>
#include <string>

using namespace mass;


void decode(double dt,
        double dx,
        double reset,
        FuncType rk,
        int n_rk,
        FuncType sk,
        int n_sk,
        int n_mixtures,
        std::vector<std::vector<double>> stims,
        std::vector<double> sim_params,
        std::vector<double> weights,
        std::vector<double> decoding_params,
        std::vector<double> decoding_lb,
        std::vector<double> decoding_ub,
        int maxeval,
        int n_spike_trains,
        const LIFMethod mm,
        std::vector<int> opt_nums,
        std::vector<DecodingMethod> dms,
        std::string folder,
        std::ofstream& oftime,
        std::ofstream& ofresult,
        std::ofstream& oftest,
        bool allinit = false
        ) {

    double p_dt = 0.001;

    ProbMixResponseLIF lif(dt, dx, reset, rk, n_rk, sk, n_sk, n_mixtures);
    lif.set_decoding_for_mixture(true);
    lif.set_lif_method(mm);
    lif.set_folder(folder);
    lif.set_stimuli(&stims);
    lif.set_parameters(sim_params);
    lif.set_ftol(1e-5);
    lif.set_algorithm(nlopt::LN_NELDERMEAD);
    lif.set_algorithm_global(nlopt::GN_DIRECT);
    lif.set_maxeval(maxeval);
    lif.set_maxeval_global(0);

    lif.get_stimulus_kernel()->set_properties(&p_dt);

    // simulate data on single stimulus
    std::vector<std::vector<double>> ss;
    std::vector<std::vector<double>> stims_n;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    // simulate training stimuli
    std::vector<std::vector<double>> ss_pm;
    std::vector<std::vector<int>> id_pm = {{0, 1, 2, 3}, {4, 5, 6, 7, 8, 9}, {10, 11, 12, 13, 14, 15}};
    for (int i = 0; i < n_mixtures; ++i) {
        int n = std::round(weights[i] * n_spike_trains);
        for (int j = 0; j < n; ++j) {
            std::vector<double> st;
            std::vector<double> x;
            lif.simulate(i, 0.0001, 4+j*1.0/(n-1), 1000, x, st, 
                    folder+"/simx.out_"+std::to_string(i)+"_"+std::to_string(j), 
                    folder+"/sims.out_"+std::to_string(i)+"_"+std::to_string(j));
            ss_pm.push_back(st);
        }
    }
    // simulate testing stimuli
    std::vector<std::vector<double>> ss_test;
    std::vector<std::vector<int>> id_test = {{0, 1}, {2, 3, 4}, {5, 6, 7}};
    for (int i = 0; i < n_mixtures; ++i) {
        int n = std::round(weights[i] * n_spike_trains / 2);
        for (int j = 0; j < n; ++j) {
            std::vector<double> st;
            std::vector<double> x;
            lif.simulate(i, 0.0001, 4+j*1.0/(n-1), 1000, x, st, 
                    folder+"/simx.test.out_"+std::to_string(i)+"_"+std::to_string(j), 
                    folder+"/sims.test.out_"+std::to_string(i)+"_"+std::to_string(j));
            ss_test.push_back(st);
        }
    }

    // decoding
    lif.set_spike_trains(&ss_pm);
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> optimization_time, decoding_time;

    for (int opt_num : opt_nums) {
        lif.set_opt_trial_num(opt_num);
        for (auto dmmethod : dms) {
            lif.set_decoding_method(dmmethod);
            lif.set_decoding_parameters(decoding_params);
            lif.set_lower_bounds(decoding_lb);
            lif.set_upper_bounds(decoding_ub);
            lif.set_maxeval(maxeval);
            lif.set_maxeval_global(0);
            lif.set_kmpp_trial_num(5);
            lif.set_ldci_trial_num(5);

            if (dmmethod == DM_MARGINAL) {
                lif.set_maxeval(maxeval*10);
            }
            
            if (dmmethod == DM_KMEANS) {
                std::cout << "###########optimal bayes" << std::endl;
                auto test = lif.calc_optimal_bayes(ss_pm, id_pm, stims);
                std::cout << test.first << "\t" << test.second << std::endl;
                oftest << test.first << "\t" << test.second << std::endl;
            }

            //auto test = lif.predict(ss_test, id_test);
            //std::cout << test.first << "\t" << test.second << "\t";
            //oftest << test.first << "\t" << test.second << "\t";

            //auto test = lif.calc_performance(id_pm);
            //std::cout << test.first << "\t" << test.second << "\t";
            //oftest << test.first << "\t" << test.second << std::endl;

            //test = lif.predict(ss_pm, id_pm);
            //std::cout << test.first << "\t" << test.second << std::endl;
            //oftest << test.first << "\t" << test.second << std::endl;


            if (dmmethod == DM_KMEANS || dmmethod == DM_MARGINAL || dmmethod == DM_EM) {
                for (auto initm : {INIT_RAND, INIT_KMPP, INIT_LDCI}) {
                    lif.set_kmpp_trial_num(5);
                    lif.set_ldci_trial_num(5);
                    lif.set_init(initm);

                    lif.set_decoding_parameters(decoding_params);
                    lif.set_maxeval(maxeval);
                    lif.set_maxeval_global(0);
                    // start time
                    start = std::chrono::system_clock::now();
                    lif.decode();
                    // end time
                    end = std::chrono::system_clock::now();

                    std::cout << "######Method " << dmmethod << "with:" << initm << std::endl;
                    lif.print_decode_result(std::cout);
                    // decoding time
                    decoding_time = end-start;
                    std::cout << "decoding time: " << decoding_time.count() << std::endl;

                    oftime << decoding_time.count() << "\t";
                    ofresult << "# Method " << dmmethod << " with " << initm << std::endl;
                    lif.print_decode_result(ofresult);

                    auto test = lif.calc_performance(id_pm);
                    std::cout << test.first << "\t" << test.second << std::endl;
                    oftest << test.first << "\t" << test.second << std::endl;
                }
            }

            if (dmmethod == DM_LL_EM1) {
                lif.set_maxeval(maxeval);
                lif.set_maxeval_global(0);
                // start time
                start = std::chrono::system_clock::now();
                lif.decode();
                // end time
                end = std::chrono::system_clock::now();

                std::cout << "\n######Method " << dmmethod << std::endl;
                lif.print_decode_result(std::cout);
                // decoding time
                auto new_decoding_time = decoding_time;
                new_decoding_time += end-start;
                std::cout << "decoding time: " << new_decoding_time.count() << std::endl;

                oftime << new_decoding_time.count() << "\t";
                ofresult << "# Method " << dmmethod << std::endl;
                lif.print_decode_result(ofresult);

                auto test = lif.calc_performance(id_pm);
                std::cout << test.first << "\t" << test.second << std::endl;
                oftest << test.first << "\t" << test.second << std::endl;
            }
        }
        oftime << std::endl;
    }
}

void monte_carlo_matrix(double dt,
        double dx,
        double reset,
        FuncType rk,
        int n_rk,
        FuncType sk,
        int n_sk,
        int n_mixtures,
        std::vector<std::vector<double>> stims,
        std::vector<double> sim_params,
        std::vector<double> weights,
        std::vector<double> decoding_params,
        std::vector<double> decoding_lb,
        std::vector<double> decoding_ub,
        int maxeval,
        int n_spike_trains,
        const LIFMethod mm,
        std::string folder,
        std::ofstream& of) {

    double p_dt = 0.001;

    ProbMixResponseLIF lif(dt, dx, reset, rk, n_rk, sk, n_sk, n_mixtures);
    lif.set_decoding_for_mixture(true);
    lif.set_lif_method(mm);
    lif.set_folder(folder);
    lif.set_stimuli(&stims);
    lif.set_parameters(sim_params);
    lif.set_ftol(1e-5);
    lif.set_algorithm(nlopt::LN_NELDERMEAD);
    lif.set_algorithm_global(nlopt::GN_DIRECT);
    lif.set_maxeval(maxeval);
    lif.set_maxeval_global(0);

    lif.get_stimulus_kernel()->set_properties(&p_dt);

    // simulate data on single stimulus
    std::vector<std::vector<double>> ss;
    std::vector<std::vector<double>> stims_n;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    // simulate training stimuli
    std::vector<std::vector<double>> ss_pm;
    for (int i = 0; i < n_mixtures; ++i) {
        int n = std::round(weights[i] * n_spike_trains);
        for (int j = 0; j < n; ++j) {
            std::vector<double> st;
            std::vector<double> x;
            lif.simulate(i, 0.0001, 4+dis(gen)*2, 1000, x, st, 
                    folder+"/simx.out_"+std::to_string(i)+"_"+std::to_string(j), 
                    folder+"/sims.out_"+std::to_string(i)+"_"+std::to_string(j));
            ss_pm.push_back(st);
        }
    }

    // decoding
    lif.set_spike_trains(&ss_pm);
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> optimization_time, decoding_time;

    lif.set_decoding_parameters(decoding_params);
    lif.set_lower_bounds(decoding_lb);
    lif.set_upper_bounds(decoding_ub);
    lif.set_maxeval(maxeval);
    lif.set_maxeval_global(0);

    lif.monte_carlo_matrix(of);
}


std::vector<std::vector<double>> ldctest(double dt,
        double dx,
        double reset,
        FuncType rk,
        int n_rk,
        FuncType sk,
        int n_sk,
        int n_mixtures,
        std::vector<std::vector<double>> stims,
        std::vector<double> sim_params,
        std::vector<double> weights,
        std::vector<double> decoding_params,
        std::vector<double> decoding_lb,
        std::vector<double> decoding_ub,
        int maxeval,
        int n_spike_trains,
        const LIFMethod mm,
        std::string folder,
        int opt_num) {

    double p_dt = 0.001;

    ProbMixResponseLIF lif(dt, dx, reset, rk, n_rk, sk, n_sk, n_mixtures);
    lif.set_decoding_for_mixture(true);
    lif.set_lif_method(mm);
    lif.set_opt_trial_num(opt_num);
    lif.set_folder(folder);
    lif.set_stimuli(&stims);
    lif.set_parameters(sim_params);
    lif.set_ftol(1e-5);
    lif.set_algorithm(nlopt::LN_NELDERMEAD);
    lif.set_algorithm_global(nlopt::GN_DIRECT);
    lif.set_maxeval(maxeval);
    lif.set_maxeval_global(0);

    lif.get_stimulus_kernel()->set_properties(&p_dt);

    // simulate data on single stimulus
    std::vector<std::vector<double>> ss;
    std::vector<std::vector<double>> stims_n;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    // simulate training stimuli
    std::vector<std::vector<double>> ss_pm;
    for (int i = 0; i < n_mixtures; ++i) {
        int n = std::round(weights[i] * n_spike_trains);
        for (int j = 0; j < n; ++j) {
            std::vector<double> st;
            std::vector<double> x;
            lif.simulate(i, 0.0001, 4+dis(gen)*2, 1000, x, st, 
                    folder+"/simx.out_"+std::to_string(i)+"_"+std::to_string(j), 
                    folder+"/sims.out_"+std::to_string(i)+"_"+std::to_string(j));
            ss_pm.push_back(st);
        }
    }

    // decoding
    lif.set_spike_trains(&ss_pm);
    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> optimization_time, decoding_time;

    lif.set_decoding_parameters(decoding_params);
    lif.set_lower_bounds(decoding_lb);
    lif.set_upper_bounds(decoding_ub);
    lif.set_maxeval(maxeval);
    lif.set_maxeval_global(0);

    return lif.ldctest(6);
}

void decode(std::vector<int> opt_nums, int rep_num, std::vector<DecodingMethod> dms, std::string str, bool allinit = false) {
    std::vector<std::vector<std::vector<double>>> decode_sin_strength = {
        {{5, 1, 0, 60}, {1, 1, 1, 65}, {6, 4, 2, 60}},
    };
    std::vector<std::vector<double>> decode_sin_weight = {
        {4.0/16, 6.0/16, 6.0/16},
    };

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    std::ofstream oftime("decode_time"+str+".txt");
    std::ofstream ofresult("decode_result"+str+".txt");
    std::ofstream oftest("decode_test"+str+".txt");

    for (int i = 0; i < rep_num; ++i) {
        decode(0.002, 0.02, 0.4,     // dx, dt, reset
                mass::RK_EXP, 0, mass::SK_SIN, 0,
                decode_sin_strength[0].size(),                      // number of mixtures
                decode_sin_strength[0],
                { 100,  0.5, 1,   50,  -25,   -40,   -15,   0.4 },  // simparams
                decode_sin_weight[0],
                { 1 + dis(gen)*6, 2+dis(gen)*2, dis(gen)*2, 60+dis(gen)*5},     // decoding params
                { 0, 0, 0, 0},     // decoding params lb
                { 50, 50, 2*M_PI, 100},     // decoding params ub
                1000,                      // maxeval
                16,                     // number of spike trains for simulation
                FK_CDF,
                opt_nums,
                dms,
                "cluster_result/"+std::to_string(i),
                oftime, ofresult, oftest, allinit);
    }
}

void monte_carlo_matrix() {
    //std::vector<std::vector<std::vector<double>>> decode_sin_strength = {
    //    {{5, 1, 0, 60}, {5, 2, 1, 60}, {10, 4, 2, 60}},
    //};
    std::vector<std::vector<std::vector<double>>> decode_sin_strength = {
        {{5, 1, 0, 60}, {1, 1, 1, 65}, {6, 4, 2, 60}},
    };
    std::vector<std::vector<double>> decode_sin_weight = {
        {4.0/16, 6.0/16, 6.0/16},
    };

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    std::ofstream of("mc_matrix.txt");

    for (int i = 0; i < 1; ++i) {
        monte_carlo_matrix(0.002, 0.02, 0.4,     // dx, dt, reset
                mass::RK_EXP, 0, mass::SK_SIN, 0,
                decode_sin_strength[0].size(),                      // number of mixtures
                decode_sin_strength[0],
                { 100,  0.5, 1,   50,  -25,   -40,   -15,   0.4 },  // simparams
                decode_sin_weight[0],
                { 1 + dis(gen)*6, 2+dis(gen)*2, 0.5+dis(gen), 60+dis(gen)*5},     // decoding params
                { 0, 0, 0, 0},     // decoding params lb
                { 50, 50, 2*M_PI, 100},     // decoding params ub
                2000,                      // maxeval
                16,                     // number of spike trains for simulation
                FK_CDF,
                "mc_matrix_result/"+std::to_string(0),
                of);
    }
}

void ldctest(int n, int opt_num, std::string outputfile,
        std::vector<std::vector<std::vector<double>>> decode_sin_strength) {
    std::vector<std::vector<double>> decode_sin_weight = {
        {4.0/16, 6.0/16, 6.0/16},
    };

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    std::vector<std::vector<std::vector<double>>> results(n);

#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (int i = 0; i < n; ++i) {
        std::cout << i << std::endl;
        results[i] = ldctest(0.002, 0.02, 0.4,     // dx, dt, reset
                mass::RK_EXP, 0, mass::SK_SIN, 0,
                decode_sin_strength[0].size(),                      // number of mixtures
                decode_sin_strength[0],
                { 100,  0.5, 1,   50,  -25,   -40,   -15,   0.4 },  // simparams
                decode_sin_weight[0],
                { 1 + dis(gen)*6, 2+dis(gen)*2, 0.5+dis(gen), 60+dis(gen)*5},     // decoding params
                { 0, 0, 0, 0},     // decoding params lb
                { 50, 50, 2*M_PI, 100},     // decoding params ub
                1000,                      // maxeval
                16,                     // number of spike trains for simulation
                FK_CDF,
                "ldctest_result/"+std::to_string(i),
                opt_num);
    }
    std::ofstream of(outputfile);

    for (auto& r1 : results) {
        for (auto& r2 : r1) {
            for (auto r3 : r2) {
                of << r3 << "\t";
            }
            of << std::endl;
        }
    }
}

void output_likelihood(double dt,
        double dx,
        double reset,
        FuncType rk,
        int n_rk,
        FuncType sk,
        int n_sk,
        int n_mixtures,
        std::vector<std::vector<double>> stims,
        std::vector<double> sim_params,
        std::string folder,
        std::ofstream& of
        ) {

    double p_dt = 0.001;

    ProbMixResponseLIF lif(dt, dx, reset, rk, n_rk, sk, n_sk, n_mixtures);
    lif.set_lif_method(FK_CDF);
    lif.set_folder(folder);
    lif.set_stimuli(&stims);
    lif.set_parameters(sim_params);
    lif.set_ftol(1e-5);
    lif.set_algorithm(nlopt::LN_NELDERMEAD);
    lif.set_algorithm_global(nlopt::GN_DIRECT);
    lif.set_decoding_method(DM_MARGINAL);

    lif.get_stimulus_kernel()->set_properties(&p_dt);

    // simulate data on single stimulus
    std::vector<std::vector<double>> ss;
    std::vector<std::vector<double>> stims_n;

    // simulate training stimuli
    std::vector<std::vector<double>> ss_pm;
    std::vector<double> st;
    std::vector<double> x;
    int i = 0;
    int j = 0;
    lif.simulate(i, 0.0001, 4, 1000, x, st, 
            folder+"/simx.out_"+std::to_string(i)+"_"+std::to_string(j), 
            folder+"/sims.out_"+std::to_string(i)+"_"+std::to_string(j));
    ss_pm.push_back(st);

    lif.set_spike_trains(&ss_pm);

    std::vector<double> decoding_params = {1., 2., 0., 60.};
    for (double p2 = 0; p2 < 10; p2 += 0.1) {
        std::cout << p2 << std::endl;
        for (double p3 = 55; p3 < 65; p3 += 0.1) {
            decoding_params = {p2,1.,0.,p3};
            lif.set_decoding_parameters(decoding_params);
            double v = lif.calc_obj_func_decode();
            of << v << "\t";
        }
        of << std::endl;
    }
}

void likelihood() {
    std::ofstream of("llk2.txt");
    output_likelihood(0.002, 0.02, 0.4, mass::RK_EXP,0,mass::SK_SIN, 0,
            1,
            {{5, 1, 0, 60}},
            { 100,  0.5, 1,   50,  -25,   -40,   -15,   1.0 },  // simparams
            "cluster_result",
            of);
}

int main(int argc, char** argv) {
    // same as decoding experiment
    std::vector<std::vector<std::vector<double>>> decode_sin_strength1 = {
        {{5, 1, 0, 60}, {1, 1, 1, 65}, {6, 4, 2, 60}},
    };
    ldctest(100, 2, "lldmatrix_2_same.txt", decode_sin_strength1);

    // similar stimuli
    std::vector<std::vector<std::vector<double>>> decode_sin_strength2 = {
        {{2, 1, 0, 62}, {3, 1, 1, 62}, {3, 4, 2, 62}},
    };
    ldctest(100, 2, "lldmatrix_2_similarstim.txt", decode_sin_strength2);

    // distinct stimuli
    std::vector<std::vector<std::vector<double>>> decode_sin_strength3 = {
        {{5, 1, 0, 55}, {2, 1, 1, 67}, {3, 4, 2, 60}},
    };
    ldctest(100, 2, "lldmatrix_2_distinct.txt", decode_sin_strength3);

    //monte_carlo_matrix();

    // run the decoding for different methods and different repetitions
    std::vector<DecodingMethod> dmsp = {
            DM_MARGINAL, 
            DM_EM, 
            DM_KMEANS,
            //DM_LL_EM1
    };

    std::vector<int> opt_nums = {1, 2, 5};
    decode(opt_nums, 5, dmsp, "_mar20_"+std::string(argv[1]), true);

}
