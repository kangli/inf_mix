#include <mass/DiscreteDecode.h>

using namespace std;
using namespace mass;


extern "C" {

Stimulus* stimulus() {
    return new Stimulus();
}

void stim_set_stim(Stimulus* p, int* stim) {
    p->set_stim(stim);
}

void stim_calc(Stimulus* p, double* t, int len, double* r) {
    double* res = (*p)(t, len);
    for (int i = 0; i < len; ++i) {
        r[i] = res[i];
    }
}

PointProcess* point_process(Stimulus* p, int len) {
    return new PointProcess(p, len);
}

void pp_generate_data(PointProcess* pp, int* data) {
    auto ss = pp->generate_data();
    pp->print_data(ss);
    for (int i = 0; i < pp->m_length; ++i) {
        data[i] = ss[i];
    }
}

void vec2arr(std::vector<double>* in, double* out, int len) {
    for (int i = 0; i < len; ++i) {
        out[i] = (*in)[i];
    }
}

void vec2arri(std::vector<int>* in, int* out, int len) {
    for (int i = 0; i < len; ++i) {
        out[i] = (*in)[i];
    }
}

void arr2vec(double* in, std::vector<double>* out, int len) {
    out->assign(in, in+len);
    for (auto i : (*out)) {
        std::cout << i;
    }
    std::cout << std::endl;
}

std::vector<double>* new_vec() {
    return new std::vector<double>();
}


void run(int n, int* true_stim, double* true_mllk, int* min_stim, double* min_mllk ) {
    Stimulus* stim = stimulus();

    //std::vector<int> stimseq{0,1,1,1,2,4,2,0,5};
    stim_set_stim(stim, true_stim);

    PointProcess* pp = point_process(stim, 500);

    auto ss = pp->generate_data(n);

    true_mllk[0] = pp->calc_mllk(ss);

    SimulatedAnnealing sa(pp);
    sa.run(ss);

    vec2arri(&(sa.m_min_stim), min_stim, 16);
    min_mllk[0] = sa.m_min_mllk;
}


void check_inits(int* s1, int* s2, int* s3,
                 int* a1, int* a2, int* a3,
                 int* b1, int* b2, int* b3,
                 int* c1, int* c2, int* c3,
                 int* a4, int* a5, int* a6,
                 int* b4, int* b5, int* b6,
                 int* c4, int* c5, int* c6) {
    std::vector<int> stim1, stim2, stim3;
    stim1.assign(s1, s1+16);
    stim2.assign(s2, s2+16);
    stim3.assign(s3, s3+16);

    Stimulus* stim = stimulus();
    PointProcess* pp = point_process(stim, 500);

    pp->mp_stim->set_stim(stim1);
    auto ss1 = pp->generate_data(1);
    pp->mp_stim->set_stim(stim2);
    auto ss2 = pp->generate_data(8);
    pp->mp_stim->set_stim(stim3);
    auto ss3 = pp->generate_data(4);

    ss1.insert(ss1.end(), ss2.begin(), ss2.end());
    ss1.insert(ss1.end(), ss3.begin(), ss3.end());

    SimulatedAnnealing sa(pp);
    HEM hem(ss1, 3, &sa);

    auto init1 = hem.random_init();
    auto init2 = hem.kmpp();
    auto init3 = hem.ldci().stim;

    for (int i = 0; i < 16; ++i) a1[i] = init1[0][i];
    for (int i = 0; i < 16; ++i) a2[i] = init1[1][i];
    for (int i = 0; i < 16; ++i) a3[i] = init1[2][i];

    for (int i = 0; i < 16; ++i) b1[i] = init2[0][i];
    for (int i = 0; i < 16; ++i) b2[i] = init2[1][i];
    for (int i = 0; i < 16; ++i) b3[i] = init2[2][i];

    for (int i = 0; i < 16; ++i) c1[i] = init3[0][i];
    for (int i = 0; i < 16; ++i) c2[i] = init3[1][i];
    for (int i = 0; i < 16; ++i) c3[i] = init3[2][i];

    init1 = hem.run(init1).stim;
    init2 = hem.run(init2).stim;
    init3 = hem.run(init3).stim;

    for (int i = 0; i < 16; ++i) a4[i] = init1[0][i];
    for (int i = 0; i < 16; ++i) a5[i] = init1[1][i];
    for (int i = 0; i < 16; ++i) a6[i] = init1[2][i];

    for (int i = 0; i < 16; ++i) b4[i] = init2[0][i];
    for (int i = 0; i < 16; ++i) b5[i] = init2[1][i];
    for (int i = 0; i < 16; ++i) b6[i] = init2[2][i];

    for (int i = 0; i < 16; ++i) c4[i] = init3[0][i];
    for (int i = 0; i < 16; ++i) c5[i] = init3[1][i];
    for (int i = 0; i < 16; ++i) c6[i] = init3[2][i];

}



// k: numer of clusters
// num: numer of repetitions
// rep: number of em trials
// data_size: number of observations; how many times 10
void testldci(
        int k, int num, int rep, int ds, double from, double to,
        double* out_mllks, double* out_init_time, double* out_em_time,
        int* out_labels, int* out_stims, int* out_true_labels, 
        bool file, bool noisy) {

    std::vector<double> mllks(num*4);
    std::vector<double> init_time(num*4);
    std::vector<double> em_time(num*4);
    std::vector<std::vector<int>> labels(num*4);
    std::vector<std::vector<std::vector<int>>> stims(num*4);
    std::vector<std::vector<int>> true_labels(num);

    std::vector<std::vector<std::vector<double>>> llds(num);

#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (int i = 0; i < num; ++i) {
        std::cout << i << std::endl;
        Stimulus stim;
        PointProcess pp(&stim, 500);
        SimulatedAnnealing sa(&pp);

        random_device rd;  
        mt19937 gen(rd()); 
        uniform_int_distribution<> dis_s(std::lround(from*ds), std::lround(to*ds));
        uniform_int_distribution<> dis_l(490, 510);
        uniform_real_distribution<> dis_u(0.0, 1.0);

        vector<vector<int>> random_stims(k);
        vector<int> random_sizes(k, 0);
        vector<vector<int>> random_data;
        generate(random_stims.begin(), random_stims.end(), [&]() {
            return sa.random_stim();
        });
        generate(random_sizes.begin(), random_sizes.end()-1, [&]() {
            return dis_s(gen);
        });
        random_sizes.back() = ds*k;
        int inc = ds;
        for (int j = 1; j < k-1; ++j) {
            random_sizes[j] += inc;
            inc += ds;
        }
        adjacent_difference(random_sizes.begin(), random_sizes.end(),
                random_sizes.begin());

        for (int j = 0; j < k; ++j) {
            int sz = random_sizes[j];

            generate_n(back_inserter(random_data), sz, [&]() {
                if (noisy) {
                    pp.mp_stim->set_stim(sa.random_stim());
                } else {
                    auto mod_stim = random_stims[j];
                    if (dis_u(gen) < 0.01) sa.modify_stim(mod_stim, 1);
                    pp.mp_stim->set_stim(mod_stim);
                }
                pp.m_length = dis_l(gen);
                true_labels[i].push_back(j);
                return pp.generate_data();
            });
        }


        HEM hem(random_data, k, &sa);

        std::vector<std::vector<int>> stims;

        hem.ld_matrix(llds[i], stims);
    }

    std::string suffix = std::to_string(k)+"_"+std::to_string(rep)+"_"+std::to_string(ds);
    std::ofstream of("ldmatrix_"+suffix+"_discrete.txt");
    for (auto lld1 : llds) {
        for (auto lld2 : lld1) {
            for (auto lld3 : lld2) {
                of << lld3 << "\t";
            }
            of << std::endl;
        }
    }

    std::ofstream oftruelabels("ldmatrix_true_labels_"+suffix+"_discrete.txt");
    for (auto& l : true_labels) {
        for (auto ll : l) {
            oftruelabels << ll << "\t";
        }
        oftruelabels << std::endl;
    }

/*
        //auto rs = hem.ldcitest();

        //rs = hem.run_debug(4);

        for ( int j = 0; j < 4; ++j) {
            mllks[i*4+j] = rs[j].mllk;
            init_time[i*4+j] = rs[j].init_time;
            em_time[i*4+j] = rs[j].em_time;
            labels[i*4+j] = rs[j].label;
            stims[i*4+j] = rs[j].stim;
        }

    }

    if (file) {
        std::string suffix = std::to_string(k)+"_"+std::to_string(rep)+"_"+std::to_string(ds);
        std::ofstream ofmllk("ldmatrix_mllk_"+suffix+".txt");
        std::ofstream ofinit_time("ldmatrix_init_time_"+suffix+".txt");
        std::ofstream ofem_time("ldmatrix_em_time_"+suffix+".txt");
        std::ofstream oflabels("ldmatrix_labels_"+suffix+".txt");
        std::ofstream oftruelabels("ldmatrix_true_labels_"+suffix+".txt");
        std::ofstream ofstims("ldmatrix_stims_"+suffix+".txt");

        for (int i = 0; i < num*4; ++i)
            ofmllk << mllks[i] << "\t";

        for (int i = 0; i < num*4; ++i)
            ofinit_time << init_time[i] << "\t";

        for (int i = 0; i < num*4; ++i)
            ofem_time << em_time[i] << "\t";
        

        int i = 0;
        for (auto& l : labels) {
            for (auto ll : l) {
                oflabels << ll << "\t";
                ++i;
            }
        }

        i = 0;
        for (auto& l : true_labels) {
            for (auto ll : l) {
                oftruelabels << ll << "\t";
                ++i;
            }
        }

        i = 0;
        for (auto& s : stims) {
            for (auto& ss : s) {
                for (auto sss : ss) {
                    ofstims << sss << "\t";
                    ++i;
                }
            }
        }

    } else {

        for (int i = 0; i < num*4; ++i)
            out_mllks[i] = mllks[i];

        for (int i = 0; i < num*4; ++i)
            out_init_time[i] = init_time[i];

        for (int i = 0; i < num*4; ++i)
            out_em_time[i] = em_time[i];

        int i = 0;
        for (auto& l : labels) {
            for (auto ll : l) {
                out_labels[i] = ll;
                ++i;
            }
        }

        i = 0;
        for (auto& l : true_labels) {
            for (auto ll : l) {
                out_true_labels[i] = ll;
                ++i;
            }
        }

        i = 0;
        for (auto& s : stims) {
            for (auto& ss : s) {
                for (auto sss : ss) {
                    out_stims[i] = sss;
                    ++i;
                }
            }
        }
    }
    */

}


// k: numer of clusters
// num: numer of repetitions
// rep: number of em trials
// data_size: number of observations; how many times 10
void test2(
        int k, int num, int rep, int ds, double from, double to,
        double* out_mllks, double* out_init_time, double* out_em_time,
        int* out_labels, int* out_stims, int* out_true_labels, 
        bool file, bool noisy) {

    std::vector<double> mllks(num*4);
    std::vector<double> init_time(num*4);
    std::vector<double> em_time(num*4);
    std::vector<std::vector<int>> labels(num*4);
    std::vector<std::vector<std::vector<int>>> stims(num*4);
    std::vector<std::vector<int>> true_labels(num);

#ifdef ENABLE_OPENMP
#pragma omp parallel for schedule (dynamic)
#endif
    for (int i = 0; i < num; ++i) {
        std::cout << i << std::endl;
        Stimulus stim;
        PointProcess pp(&stim, 500);
        SimulatedAnnealing sa(&pp);

        random_device rd;  
        mt19937 gen(rd()); 
        uniform_int_distribution<> dis_s(std::lround(from*ds), std::lround(to*ds));
        uniform_int_distribution<> dis_l(490, 510);
        uniform_real_distribution<> dis_u(0.0, 1.0);

        vector<vector<int>> random_stims(k);
        vector<int> random_sizes(k, 0);
        vector<vector<int>> random_data;
        generate(random_stims.begin(), random_stims.end(), [&]() {
            return sa.random_stim();
        });
        generate(random_sizes.begin(), random_sizes.end()-1, [&]() {
            return dis_s(gen);
        });
        random_sizes.back() = ds*k;
        int inc = ds;
        for (int j = 1; j < k-1; ++j) {
            random_sizes[j] += inc;
            inc += ds;
        }
        adjacent_difference(random_sizes.begin(), random_sizes.end(),
                random_sizes.begin());

        for (int j = 0; j < k; ++j) {
            int sz = random_sizes[j];

            generate_n(back_inserter(random_data), sz, [&]() {
                if (noisy) {
                    pp.mp_stim->set_stim(sa.random_stim());
                } else {
                    auto mod_stim = random_stims[j];
                    if (dis_u(gen) < 0.01) sa.modify_stim(mod_stim, 1);
                    pp.mp_stim->set_stim(mod_stim);
                }
                pp.m_length = dis_l(gen);
                true_labels[i].push_back(j);
                return pp.generate_data();
            });
        }


        HEM hem(random_data, k, &sa);

        std::vector<HEM::Result> rs(4);
        // calibrate
        //rs[0] = hem.run(0, 1);
        // run
        rs[0] = hem.run(0, rep);
        rs[1] = hem.run(1, rep);
        rs[2] = hem.run(2, rep);
        rs[3] = hem.optimal_bayes(random_stims);

        //rs = hem.run_debug(4);

        for ( int j = 0; j < 4; ++j) {
            mllks[i*4+j] = rs[j].mllk;
            init_time[i*4+j] = rs[j].init_time;
            em_time[i*4+j] = rs[j].em_time;
            labels[i*4+j] = rs[j].label;
            stims[i*4+j] = rs[j].stim;
        }

    }

    if (file) {
        std::string suffix = std::to_string(k)+"_"+std::to_string(rep)+"_"+std::to_string(ds);
        std::ofstream ofmllk("mllk_"+suffix+".txt");
        std::ofstream ofinit_time("init_time_"+suffix+".txt");
        std::ofstream ofem_time("em_time_"+suffix+".txt");
        std::ofstream oflabels("labels_"+suffix+".txt");
        std::ofstream oftruelabels("true_labels_"+suffix+".txt");
        std::ofstream ofstims("stims_"+suffix+".txt");

        for (int i = 0; i < num*4; ++i)
            ofmllk << mllks[i] << "\t";

        for (int i = 0; i < num*4; ++i)
            ofinit_time << init_time[i] << "\t";

        for (int i = 0; i < num*4; ++i)
            ofem_time << em_time[i] << "\t";
        

        int i = 0;
        for (auto& l : labels) {
            for (auto ll : l) {
                oflabels << ll << "\t";
                ++i;
            }
        }

        i = 0;
        for (auto& l : true_labels) {
            for (auto ll : l) {
                oftruelabels << ll << "\t";
                ++i;
            }
        }

        i = 0;
        for (auto& s : stims) {
            for (auto& ss : s) {
                for (auto sss : ss) {
                    ofstims << sss << "\t";
                    ++i;
                }
            }
        }

    } else {

        for (int i = 0; i < num*4; ++i)
            out_mllks[i] = mllks[i];

        for (int i = 0; i < num*4; ++i)
            out_init_time[i] = init_time[i];

        for (int i = 0; i < num*4; ++i)
            out_em_time[i] = em_time[i];

        int i = 0;
        for (auto& l : labels) {
            for (auto ll : l) {
                out_labels[i] = ll;
                ++i;
            }
        }

        i = 0;
        for (auto& l : true_labels) {
            for (auto ll : l) {
                out_true_labels[i] = ll;
                ++i;
            }
        }

        i = 0;
        for (auto& s : stims) {
            for (auto& ss : s) {
                for (auto sss : ss) {
                    out_stims[i] = sss;
                    ++i;
                }
            }
        }
    }

}


int main() {

    // k
    // num
    // rep
    // data size
    //test2(4, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, true);



    // large data size
    /*
    for (int rep : {1, 2, 3}) {
        for (int k : {3, 4, 5}) {
            test2(k, 1000, rep, 100, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true, false);
            test2(k, 1000, rep, 101, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true, true);
        }
    }
    */

    testldci(4, 100, 1, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true, false);

    // main run
    for (int rep : {1,2,3}) {
        for (int k : {4}) {
            test2(k, 100, rep, 100, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true, false);
            //test2(k, 1000, rep, 101, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true, true);
        }
    }

    // noise
    //test2(3, 100, 1, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(3, 100, 2, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(3, 100, 3, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);

    // equal cluster size
    //test2(6, 100, 1, 10, 0.9, 1.1, NULL, NULL, NULL, NULL, NULL, NULL, true);

    // large data size
    //test2(6, 100, 1, 10, 0.6, 1.4, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(4, 100, 1, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(4, 100, 2, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(4, 100, 3, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);
    //test2(4, 100, 4, 10, 0.7, 1.3, NULL, NULL, NULL, NULL, NULL, NULL, true);

}
}
