*Kang Li and Susanne Ditlevsen. (2018). Clustering and inference in complicated mixture models.*

### Requirements

- [openCV](https://www.opencv.org/)
- [Boost](https://www.boost.org/)
- [nlopt](https://nlopt.readthedocs.io/en/latest/)
- [GSL](https://www.gnu.org/software/gsl/)

For python requirements, see ```python/requirements.txt```.

### How to use
Create the executables `cluster` and `clusterdiscrete`:

    mkdir build
    cd build
    cmake ..
    make cluster clusterdiscrete

Experiment I:

    cd build
    ./cluster

Experiment II:

    cd build
    ./clusterdiscrete

Experiment III:

    cd python
    mkdir venv
    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt
    python cluster.py

