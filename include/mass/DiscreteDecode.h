#ifndef DISCRETEDECODE_H
#define DISCRETEDECODE_H

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/ml.hpp"

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <random>
#include <algorithm>
#include <tuple>
#include <map>

#include <chrono>
#include <ctime>

namespace mass {

// one gamma pdf
class OneGamma {
public:
    // init with vector [w1, w2, b1, b2, shape1, scale1, shape2, scale2]
    OneGamma(const std::vector<double>& p) :
        w(p[0]), a(p[1]), b(p[2]), shape(p[3]), scale(p[4]) {}

    // init with vector [w1, w2, b1, b2, shape1, scale1, shape2, scale2]
    OneGamma(const std::vector<double>& p, int len) :
        w(p[0]), a(p[1]), b(p[2]), shape(p[3]), scale(p[4]),
        v(len, 0) 
    {
        for (int i = 0; i < len; ++i) {
            double t = i * 0.001;
            v[i] = w * gsl_ran_gamma_pdf(t*a + b, shape, scale);
        }
    }

    // calculate the value for time t after stimulus pulsing
    double operator()(double t) {
        return w * gsl_ran_gamma_pdf(t*a + b, shape, scale);
    }

    // calculate the value for time t after stimulus pulsing
    double operator[](int t) {
        return v[t];
    }

private:
    double w; double a; double b;   double shape;   double scale;
    std::vector<double> v;
};


// two gamma pdfs
class TwoGammas {
public:
    // init with vector [w1, w2, b1, b2, shape1, scale1, shape2, scale2]
    TwoGammas(const std::vector<double>& p) :
        w1(p[0]), a1(p[1]), b1(p[2]), shape1(p[3]), scale1(p[4]),
        w2(p[5]), a2(p[6]), b2(p[7]), shape2(p[8]), scale2(p[9]) {
    }

    // calculate the value for time t after stimulus pulsing
    double operator()(double t) {
        return w1 * gsl_ran_gamma_pdf(t * a1 + b1, shape1, scale1) +
            w2 * gsl_ran_gamma_pdf(t * a2 + b2, shape2, scale2);
    }

private:
    double w1;  double a1;  double b1;  double shape1; double scale1;
    double w2;  double a2;  double b2;  double shape2; double scale2;
};


// input
class Stimulus {
public:
    Stimulus() :
        m_num_stim(16),
        //m_weights{0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1},
        m_weights{0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.1},
        m_stim(m_num_stim, 0)
    {
        m_gammas.push_back(OneGamma({1.0, 10, -0.1, 3.0, 1.0/3.0}, 650));
        m_gammas.push_back(OneGamma({0.5, 5, 0.0, 10.0, 0.1}, 650));
        m_gammas.push_back(OneGamma({1.0, 10, 0.0, 4.0, 0.5}, 650));
        m_gammas.push_back(OneGamma({-1.0, 10, 0.0, 5.0, 0.2}, 650));
        m_gammas.push_back(OneGamma({-0.5, 5, -0.5, 5.0, 0.2}, 650));
        m_gammas.push_back(OneGamma({-1.0, 10, 0.0, 3.0, 1.0}, 650));

        m_gammas.push_back(OneGamma({-0.2, 100, 0, 1, 1.0/1.2}, 50));
        m_gammas.push_back(OneGamma({0.2, 100, 0, 2, 2}, 50));
    }

    void set_stim(int* stim) {
        for (int i = 0; i < m_num_stim; ++i) {
            m_stim[i] = stim[i];
        }
    }

    void set_stim(std::vector<int> stim) {
        m_stim = stim;
    }

    double operator()(double t) {
        double r = 0;
        for (int i = 0; i < m_num_stim; ++i) {
            r += m_gammas[m_stim[i]](t) * m_weights[i];
        }
        return r;
    }

    double* operator()(double* t, int len) {
        double* res = new double[len];
        for (int j = 0; j < len; ++j) {
            res[j] = (*this)(t[j]);
        }
        return res;
    }

    double operator()(int t) {
        double r = 0;
        for (int i = 0; i < m_num_stim; ++i) {
            r += m_gammas[m_stim[i]][t] * m_weights[i];
        }
        return r;
    }

    double* operator()(int* t, int len) {
        double* res = new double[len];
        for (int j = 0; j < len; ++j) {
            res[j] = (*this)(t[j]);
        }
        return res;
    }


    std::vector<double> operator()(std::vector<double> t) {
        int len = t.size();
        std::vector<double> res(len);
        for (int j = 0; j < len; ++j) {
            res[j] = (*this)(t[j]);
        }
        return res;
    }

    std::vector<double> operator()(std::vector<int> t) {
        int len = t.size();
        std::vector<double> res(len);
        for (int j = 0; j < len; ++j) {
            res[j] = (*this)(t[j]);
        }
        return res;
    }

    double response(const std::vector<int>& ss, int t) {
        double res = 0;
        for (int i = t-1; i >= 0 && t-i<=50; --i) {
            if (ss[i] == 1) {
                res += m_gammas[6][t - i] + m_gammas[7][t-i];
            }
        }
        return res;
    }

private:
    int m_num_stim;
    int m_num_diff_stim = 6;
    std::vector<double> m_weights;
    std::vector<OneGamma> m_gammas;
    std::vector<int> m_stim;
};


// point process model
class PointProcess {
public:

    PointProcess(Stimulus* p_stim, int len)
        : mp_stim(p_stim), m_length(len) {
    }

    // calculate firing rate increment
    double calc_rate_inc(double t) {
        double s = (*mp_stim)(t);
        return m_base_rate_inc + m_stim_scale*s;
    }

    // calculate firing rate increment
    double calc_rate_inc(int t) {
        double s = (*mp_stim)(t);
        return m_base_rate_inc + m_stim_scale*s;
    }

    // Generate new data
    std::vector<int> generate_data() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::bernoulli_distribution d(0.0);
        std::vector<int> spike_train(m_length);
        double rate = m_start;
        for (int i = 0; i < m_length; ++i) {
            rate += calc_rate_inc(i);
            rate += mp_stim->response(spike_train, i);
            if (rate >= 1.0-m_ro) rate = 1.0-m_ro;
            if (rate <= m_ro) rate = m_ro;
            if (std::isnan(rate)) rate = m_ro;

            /*
            double sd = rate < 0.5 ? rate : 1-rate;
            std::normal_distribution<> rn(0, sd*0.5);
            double frate = rate + rn(gen);
            if (frate >= 1.0-m_ro) frate = 1.0-m_ro;
            if (frate <= m_ro) frate = m_ro;
            */

            d.param(std::bernoulli_distribution::param_type(rate));
            if (d(gen)) {
                spike_train[i] = 1;
                rate = m_start;
            } else {
                spike_train[i] = 0;
            }
        }
        return spike_train;
    }

    // Generate new data
    std::vector<std::vector<int>> generate_data(int n) {
        std::vector<std::vector<int>> ss;
        for (int i = 0; i < n; ++i) {
            ss.push_back(generate_data());
        }
        return ss;
    }

    void print_data(const std::vector<int>& ss) {
        for (auto it = ss.begin(); it != ss.end(); ++it) {
            std::cout << *it;
        }
        std::cout << std::endl;
    }

    // cross entropy
    double calc_ce(const std::vector<int>& ss) {
        return calc_mllk(ss) / ss.size();
    }


    // calcualte minus log likelihood value
    double calc_mllk(const std::vector<int>& ss) {
        double res = 0.0;
        double rate = m_start;
        int sz = ss.size();
        for (int i = 0; i < sz; ++i) {
            rate += calc_rate_inc(i);
            rate += mp_stim->response(ss, i);
            if (rate >= 1.0-m_ro) rate = 1.0-m_ro;
            if (rate <= m_ro) rate = m_ro;
            if (std::isnan(rate)) rate = m_ro;
            if (ss[i] == 1) {
                res -= std::log(rate);
                rate = m_start;
            } else {
                res -= std::log(1-rate);
            }
            // debug
            if ( std::isnan(res)) {
                std::cout << rate << std::endl;
                std::cout << std::log(rate) << std::endl;
                std::cout << "na mllk" << std::endl;
            }
        }
        return res;
    }

    double calc_mllk(const std::vector<int>* ss) {
        return calc_mllk(*ss);
    }

    double calc_mllk(const std::vector<std::vector<int>>& ss) {
        double r = 0;
        for (auto& s : ss) {
            r += calc_mllk(s);
        }
        return r;
    }

    double calc_mllk(const std::vector<std::vector<int>>* ss) {
        double r = 0;
        for (auto& s : (*ss)) {
            r += calc_mllk(s);
        }
        return r;
    }

    Stimulus* mp_stim;
    int m_length;           // spike train length: number of ms

private:
    double m_base_rate_inc = 0.1;
    double m_stim_scale = 0.1;
    double m_ro = 1e-15;    // rate offset
    double m_start =  0.3;

};



class SimulatedAnnealing {

public:
    SimulatedAnnealing(
            PointProcess* pp)
    : mp_pp(pp), m_num_stim(16), m_num_diff_stim(6) {
    }

    std::vector<int> random_stim() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<double> w(m_num_diff_stim, 1);
        std::discrete_distribution<> d(w.begin(), w.end());

        std::vector<int> stim(m_num_stim);
        for (auto& i : stim)    
            i = d(gen);
        return stim;
    }

    void modify_stim(std::vector<int>& stim, int num) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<int> v(m_num_stim);
        std::iota(v.begin(), v.end(), 0);
        std::shuffle(v.begin(), v.end(), gen);

        std::vector<double> w(m_num_diff_stim, 1);

        for (int i = 0; i < num; ++i) {
            int id = stim[v[i]];
            w[id] = 0;
            std::discrete_distribution<> d(w.begin(), w.end());
            stim[v[i]] = d(gen);
            w[id] = 1;
        }
    }


    std::tuple<double, std::vector<int>> run(
            const std::vector<std::vector<int>>& ss,
            const std::vector<int>& init) {
        int m = m_m;      // population number in evolution
        int n = m_n;     // simulated annealing steps
        // tuple for result mllk and stim
        std::vector<std::tuple<double, std::vector<int>>> res(m*n);

        // initialization
        for (int mi = 0; mi < m; mi++) {
            std::get<1>(res[mi*n]) = mi == 0 ? init : random_stim();
            mp_pp->mp_stim->set_stim(std::get<1>(res[mi*n]));
            std::get<0>(res[mi*n]) = mp_pp->calc_mllk(ss);
        }

        // annealing repetitions
        for (double p : {0.8, 0.6, 0.4, 0.2, 0.1, 0.05}) {
            // mi is for each in total m
            for (int mi = 0; mi < m; mi++) {
                std::random_device rd;  
                std::mt19937 gen(rd());
                std::uniform_real_distribution<> d(0.0, 1.0);

                auto stim = std::get<1>(res[mi*n]);
                auto mllk = std::get<0>(res[mi*n]);
                auto old_stim = stim;
                auto old_mllk = mllk;

                // ni is for each step in total n
                for (int ni = 1; ni < n; ++ni) {
                    modify_stim(stim, 2);
                    mp_pp->mp_stim->set_stim(stim.data());
                    mllk = mp_pp->calc_mllk(ss);

                    if (mllk < old_mllk || d(gen) < p) {
                        old_stim = stim;
                        old_mllk = mllk;
                    } else {
                        stim = old_stim;
                        mllk = old_mllk;
                    }
                    std::get<0>(res[mi*n+ni]) = mllk;
                    std::get<1>(res[mi*n+ni]) = stim;
                }
            }

            // use the m best stimuli
            std::sort(res.begin(), res.end(),
                    [&](std::tuple<double, std::vector<int>> a, 
                        std::tuple<double, std::vector<int>> b) { 
                    return std::get<0>(a) < std::get<0>(b); }
            );
            std::vector<std::tuple<double, std::vector<int>>> min_res;
            std::unique_copy(res.begin(), res.end(), 
                    std::back_inserter(min_res),
                    [&](std::tuple<double, std::vector<int>> a, 
                        std::tuple<double, std::vector<int>> b) { 
                    return std::get<0>(a) == std::get<0>(b); }
            );
            int sz = min_res.size();
            for (int mi = 0; mi < m; ++mi) {
                if (mi >= sz)
                    res[mi*n] = min_res[mi % sz];
                else
                    res[mi*n] = min_res[mi];
            }
        }
        m_min_mllk = std::get<0>(res[0]);
        m_min_stim = std::get<1>(res[0]);

        return res[0];

        //std::cout << m_min_mllk;
    }

    std::tuple<double, std::vector<int>> run(
            const std::vector<std::vector<int>>& ss,
            const std::vector<int>& init, int rep) {
        std::tuple<double, std::vector<int>> min_res{HUGE_VAL, {}};
        for (int i = 0; i < rep; ++i) {
            auto res = run(ss, init);
            if (std::get<0>(res) < std::get<0>(min_res)) {
                min_res = res;
            }
        }
        return min_res;
    }

    std::tuple<double, std::vector<int>> run(
            const std::vector<std::vector<int>>& ss) {
        return run(ss, random_stim());
    }

    std::tuple<double, std::vector<int>> run(
            const std::vector<std::vector<int>>& ss,
            int rep) {
        std::tuple<double, std::vector<int>> min_res{HUGE_VAL, {}};
        for (int i = 0; i < rep; ++i) {
            auto res = run(ss);
            if (std::get<0>(res) < std::get<0>(min_res)) {
                min_res = res;
            }
        }
        return min_res;
    }

    double m_min_mllk;
    std::vector<int> m_min_stim;
    PointProcess* mp_pp;

    int m_m = 1;        // population size
    int m_n = 10;       // sa steps


private:
    int m_num_stim;
    int m_num_diff_stim;

};


// hard assigned em algorithm
class HEM {
public:
    struct Result {
        double mllk;
        std::vector<int> label;
        std::vector<std::vector<int>> stim;
        double init_time;
        double em_time;
    };
public:
    HEM(std::vector<std::vector<int>>& ss, int k, 
        SimulatedAnnealing* sa):
        m_ss(ss), m_k(k), mp_sa(sa)
    {}

    std::vector<std::vector<int>> random_init() {
        std::vector<int> v(m_ss.size());
        std::iota(v.begin(), v.end(), 0);
        std::shuffle(v.begin(), v.end(), std::mt19937{std::random_device{}()});
        std::vector<std::vector<int>> inits;

        for (int i = 0; i < m_k; ++i) {
            auto res = mp_sa->run( {m_ss[v[i]]} );
            inits.push_back(std::get<1>(res));
        }
        return inits;
    }

    // compute mllk matrix ld distance matrix
    void ld_matrix(std::vector<std::vector<double>>& mllks,
            std::vector<std::vector<int>>& stims,
            std::vector<std::vector<int>>& ss, int rep=1) {
        // fill the mllk matrix
        for (auto& s : ss) {
            auto tmp = mp_sa->run({s}, rep);
            stims.push_back(std::get<1>(tmp));
            mp_sa->mp_pp->mp_stim->set_stim(stims.back());
            mllks.push_back(std::vector<double>(ss.size()));
            std::transform(ss.begin(), ss.end(), mllks.back().begin(), 
                [&](std::vector<int> si) {
                    return mp_sa->mp_pp->calc_ce(si);
                }
            );
            // translocate
            //for(auto& llk : mllks.back()) 
            //    llk -= *std::min_element(mllks.back().begin(), mllks.back().end());
        }
    }
    // compute mllk matrix ld distance matrix
    void ld_matrix_partial(std::vector<std::vector<double>>& mllks,
            std::vector<std::vector<int>>& stims,
            std::vector<std::vector<int>>& ss,
            std::vector<std::vector<int>>& pss,
            int rep = 1) {
        // fill the mllk matrix
        for (auto& s : pss) {
            auto tmp = mp_sa->run({s});
            stims.push_back(std::get<1>(tmp));
            mp_sa->mp_pp->mp_stim->set_stim(stims.back());
            mllks.push_back(std::vector<double>(ss.size()));
            std::transform(ss.begin(), ss.end(), mllks.back().begin(), 
                [&](std::vector<int> si) {
                    return mp_sa->mp_pp->calc_ce(si);
                }
            );
            // translocate
            //for(auto& llk : mllks.back()) 
            //    llk -= *std::min_element(mllks.back().begin(), mllks.back().end());
        }
    }
    // compute mllk matrix ld distance matrix
    void ld_matrix(std::vector<std::vector<double>>& mllks,
            std::vector<std::vector<int>>& stims) {
        ld_matrix(mllks, stims, m_ss);
    }

    // compute mllk matrix ld distance matrix for nth stimuli
    void ld_matrix(std::vector<std::vector<double>>& mllks,
            std::vector<std::vector<int>>& stims, int n, 
            bool calc_stim = true, bool calc_mllk = true) {
        if (calc_stim) {
            auto& s = m_ss[n];
            auto tmp = mp_sa->run({s});
            stims[n] = (std::get<1>(tmp));
        }
        if (calc_mllk) {
            mp_sa->mp_pp->mp_stim->set_stim(stims[n]);
            std::transform(m_ss.begin(), m_ss.end(), mllks[n].begin(), 
                [&](std::vector<int> si) {
                return mp_sa->mp_pp->calc_ce(si);
                }
            );
            // translocate
            //for(auto& llk : mllks[n]) 
            //    llk -= *std::min_element(mllks[n].begin(), mllks[n].end());
        }
    }


    // kmeans++ initials
    // FIXME a subset of all stimuli if data size too big
    std::vector<std::vector<int>> kmpp() {

        // mllk matrix
        std::vector<std::vector<double>> mllks(m_ss.size(),
                std::vector<double>(m_ss.size()));
        // stims for each ss
        std::vector<std::vector<int>> stims(m_ss.size());
        // fill the mllk matrix
        //ld_matrix(mllks, stims);

        // result
        std::vector<std::vector<int>> res(m_k);

        std::random_device rd;
        std::mt19937 gen(rd());
        std::vector<double> w(m_ss.size(), 1);
        std::discrete_distribution<> d(w.begin(), w.end());
        // all selected indexes
        std::vector<int> is;
        is.push_back(d(gen));
        ld_matrix(mllks, stims, is.back(), true, false);
        res[0] = stims[is.back()];

        // fill in total m_k centers
        for (int j = 1; j < m_k; ++j) {
            if (j == 1) {
                ld_matrix(mllks, stims, is.back(), false, true);
                std::vector<double> prob;
                std::copy(mllks[is.back()].begin(), mllks[is.back()].end(),
                        std::back_inserter(prob));
                for(auto& pr : prob) 
                    pr -= *std::min_element(prob.begin(), prob.end());
                for (auto ind : is) {
                    prob[ind] = 0;
                }
                std::discrete_distribution<> dis(prob.begin(), prob.end());
                is.push_back(dis(gen));
                ld_matrix(mllks, stims, is.back(), true, false);
                res[j] = stims[is.back()];
            } else {
                std::vector<double> ws(m_ss.size());
                ld_matrix(mllks, stims, is.back(), false, true);
                int sz = m_ss.size();
                for (int k = 0; k < sz; ++k) {
                    std::vector<double> tmp;
                    for (auto ind : is) {
                        tmp.push_back(mllks[ind][k]);
                    }
                    ws[k] = *std::min_element(tmp.begin(), tmp.end());
                }
                for(auto& w : ws) 
                    w -= *std::min_element(ws.begin(), ws.end());
                for (auto ind : is) {
                    ws[ind] = 0;
                }
                std::discrete_distribution<> dis(ws.begin(), ws.end());
                is.push_back(dis(gen));
                //is.push_back(std::max_element(ws.begin(), ws.end())-ws.begin());
                ld_matrix(mllks, stims, is.back(), true, false);
                res[j] = stims[is.back()];
            }
        }
        return res;
    }


    // LDCI
    Result ldci(bool estimate = true, bool partial = false) {
        // use a subset of all data
        std::vector<std::vector<int>> part_ss;
        if (true) {
            std::vector<int> id_v(m_ss.size());
            std::iota(id_v.begin(), id_v.end(), 0);
            std::shuffle(id_v.begin(), id_v.end(), 
                    std::mt19937{std::random_device{}()});
            int psize = m_k * m_k * 3;
            if (psize > m_ss.size()) psize = m_ss.size();
            for (int i = 0; i < psize; ++i) {
                part_ss.push_back(m_ss[id_v[i]]);
            }
        } else {
            part_ss = m_ss;
        }
        // mllk matrix
        std::vector<std::vector<double>> mllks;
        // stims for each ss
        std::vector<std::vector<int>> stims;

        if (partial) {
            // fill the mllk matrix
            std::vector<std::vector<double>> mllks_raw;
            ld_matrix_partial(mllks_raw, stims, m_ss, part_ss);

            // invert
            int szc = part_ss.size();
            int szr = m_ss.size();
            for (int i = 0; i < szr; ++i) {
                mllks.push_back(std::vector<double>());
                for (int j = 0; j < szc; ++j) {
                    mllks.back().push_back(mllks_raw[j][i]);
                }
            }
        } else {
            // fill the mllk matrix
            ld_matrix(mllks, stims, part_ss, 2);

            // augument
            int sz = part_ss.size();
            for (int i = 0; i < sz; ++i) {
                for (int j = 0; j < sz; ++j) {
                    mllks[i].push_back(mllks[j][i]);
                }
            }
        }
        
        // opencv matrix
        // cross entropy
        std::vector<double> datamat;
        for (auto& i : mllks) {
            for (auto j : i) {
                datamat.push_back(j);
            }
        }
        cv::Mat samples( 
            partial ? m_ss.size() : part_ss.size(),
            partial ? part_ss.size() : 2 * part_ss.size(),
            cv::DataType<double>::type, datamat.data() 
        );
        samples.convertTo(samples, CV_32F);
        cv::Mat labels;

        /*
        cv::kmeans(samples, m_k, labels, 
                cv::TermCriteria( cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 
                                  100, 0.0001),
                10, cv::KMEANS_PP_CENTERS, cv::noArray());
                */

        cv::Ptr<cv::ml::EM> em_model = cv::ml::EM::create();
        em_model->setClustersNumber(m_k);
        auto emmethod = partial ? cv::ml::EM::COV_MAT_DIAGONAL : cv::ml::EM::COV_MAT_SPHERICAL;
        em_model->setCovarianceMatrixType(emmethod);
        em_model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 1e-8));
        em_model->trainEM( samples, cv::noArray(), labels, cv::noArray() );

        int sz = partial ? m_ss.size() : part_ss.size();
        std::vector<int> classes(sz);
        for (int i = 0; i < sz; ++i)
            classes[i] = labels.at<int>(i,0);

        std::map<int, std::vector<std::vector<int>>> clusters;
        for (int i = 0; i < sz; ++i) {
            clusters[classes[i]].push_back(partial ? m_ss[i] : part_ss[i]);
        }

        // re estimate
        // FIXME check not empty
        double mllk = 0;
        std::vector<std::vector<int>> res;
        for (auto it = clusters.begin(); it != clusters.end(); ++it) {
            auto tmp = estimate ? mp_sa->run({it->second}) :
                                  mp_sa->run({it->second[0]});
            res.push_back(std::get<1>(tmp));
            mllk += std::get<0>(tmp);
        }

        return Result{mllk, classes, res, 0, 0};
    }


    std::vector<Result> ldcitest() {
        // mllk matrix
        std::vector<std::vector<double>> mllks;
        // stims for each ss
        std::vector<std::vector<int>> stims;
        // fill the mllk matrix
        // it is backward matrix
        ld_matrix(mllks, stims, m_ss);

        // augument
        std::vector<std::vector<double>> mllks_aug = mllks;
        int sz = m_ss.size();
        for (int i = 0; i < sz; ++i) {
            for (int j = 0; j < sz; ++j) {
                mllks_aug[i].push_back(mllks_aug[j][i]);
            }
        }
        // invert
        std::vector<std::vector<double>> mllks_t = mllks;
        for (int i = 1; i < sz; ++i) {
            for (int j = 0; j < i; ++j) {
                std::swap(mllks_t[j][i], mllks_t[i][j]);
            }
        }

        // partial
        std::vector<std::vector<double>> mllks_p;
        std::vector<int> id_v(m_ss.size());
        std::iota(id_v.begin(), id_v.end(), 0);
        std::shuffle(id_v.begin(), id_v.end(), 
                std::mt19937{std::random_device{}()});
        int psize = m_k * m_k;
        if (psize > m_ss.size()) psize = m_ss.size();
        for (int j = 0; j < m_ss.size(); ++j) {
            mllks_p.push_back(std::vector<double>());
            for (int i = 0; i < psize; ++i) {
                mllks_p.back().push_back(mllks_t[j][id_v[i]]);
            }
        }

        // opencv matrix
        std::vector<double> datamat;
        for (auto& i : mllks) {
            for (auto j : i) {
                datamat.push_back(j);
            }
        }
        cv::Mat samples( sz, sz, cv::DataType<double>::type, datamat.data() );
        samples.convertTo(samples, CV_32F);
        cv::Mat labels;


        std::vector<double> datamat_aug;
        for (auto& i : mllks_aug) {
            for (auto j : i) {
                datamat_aug.push_back(j);
            }
        }
        cv::Mat samples_aug( sz, 2*sz, cv::DataType<double>::type, datamat_aug.data() );
        samples_aug.convertTo(samples_aug, CV_32F);
        cv::Mat labels_aug;

        std::vector<double> datamat_t;
        for (auto& i : mllks_t) {
            for (auto j : i) {
                datamat_t.push_back(j);
            }
        }
        cv::Mat samples_t( sz, sz, cv::DataType<double>::type, datamat_t.data() );
        samples_t.convertTo(samples_t, CV_32F);
        cv::Mat labels_t;


        std::vector<double> datamat_p;
        for (auto& i : mllks_p) {
            for (auto j : i) {
                datamat_p.push_back(j);
            }
        }
        cv::Mat samples_p( sz, psize, cv::DataType<double>::type, datamat_p.data() );
        samples_p.convertTo(samples_p, CV_32F);
        cv::Mat labels_p;
        /*
        cv::kmeans(samples, m_k, labels, 
                cv::TermCriteria( cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 
                                  100, 0.0001),
                10, cv::KMEANS_PP_CENTERS, cv::noArray());
                */

        cv::Ptr<cv::ml::EM> em_model = cv::ml::EM::create();
        em_model->setClustersNumber(m_k);
        em_model->setTermCriteria(cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 100, 1e-5));
        std::vector<Result> results;

        em_model->setCovarianceMatrixType(cv::ml::EM::COV_MAT_SPHERICAL);
        em_model->trainEM( samples, cv::noArray(), labels, cv::noArray() );
        std::vector<int> classes(sz);
        for (int i = 0; i < sz; ++i)
            classes[i] = labels.at<int>(i,0);
        results.push_back(Result{0, classes, {{}}, 0, 0});


        em_model->trainEM( samples_aug, cv::noArray(), labels_aug, cv::noArray() );
        std::vector<int> classes_aug(sz);
        for (int i = 0; i < sz; ++i)
            classes_aug[i] = labels_aug.at<int>(i,0);
        results.push_back(Result{0, classes_aug, {{}}, 0, 0});


        em_model->trainEM( samples_t, cv::noArray(), labels_t, cv::noArray() );
        std::vector<int> classes_t(sz);
        for (int i = 0; i < sz; ++i)
            classes_t[i] = labels_t.at<int>(i,0);
        results.push_back(Result{0, classes_t, {{}}, 0, 0});

        em_model->setCovarianceMatrixType(cv::ml::EM::COV_MAT_DIAGONAL);
        em_model->trainEM( samples_p, cv::noArray(), labels_p, cv::noArray() );
        std::vector<int> classes_p(sz);
        for (int i = 0; i < sz; ++i)
            classes_p[i] = labels_p.at<int>(i,0);
        results.push_back(Result{0, classes_p, {{}}, 0, 0});

        return results;
    }
    
    // debug em trace of ldci
    std::vector<Result> run_debug(int n) {
        std::vector<Result> results(n);
        results[0] = ldci();
        auto stims =results[0].stim;

        // category -> data
        std::map<int, std::vector<std::vector<int>>> clusters;
        std::vector<int> indexes(m_ss.size());

        // em loops
        int step = 0;
        while (true) {
            clusters.clear();
            // make clusters
            auto indit = indexes.begin();
            for (auto& ss : m_ss) {
                double min_mllk = HUGE_VAL;
                int min_i = 0;
                for (int i = 0; i < m_k; ++i) {
                    mp_sa->mp_pp->mp_stim->set_stim(stims[i]);
                    double mllk = mp_sa->mp_pp->calc_mllk(ss);
                    if (mllk < min_mllk) {
                        min_mllk = mllk;
                        min_i = i;
                    }
                }
                clusters[min_i].push_back(ss);
                *indit = min_i;
                ++indit;
            }

            // optimize each cluster with sa
            double mllk = 0;
            for (auto& c : clusters) {
                auto res = mp_sa->run(c.second, stims[c.first]);
                stims[c.first] = std::get<1>(res);
                mllk += std::get<0>(res);
            }

            results[step+1] = Result{mllk, indexes, stims, 0, 0};

            step ++;

            if (step > 2) break;

            /*
            std::cout << step << std::endl;
            std::cout << mllk << std::endl;
            for (auto i : indexes) {
                std::cout << i;
            }
            std::cout << std::endl;
            */
        }

        /*
        std::cout << mllk_old << "\t";
        for (auto i : indexes) {
            std::cout << i;
        }
        std::cout << std::endl;
        */

        return results;
    }

    
    Result run(const std::vector<std::vector<int>>& stims_in) {
        auto stims = stims_in;
        double mllk_old = HUGE_VAL;
        // category -> data
        std::map<int, std::vector<std::vector<int>>> clusters;
        std::vector<int> indexes(m_ss.size());

        // em loops
        int step = 0;
        while (true) {
            clusters.clear();
            // make clusters
            auto indit = indexes.begin();
            for (auto& ss : m_ss) {
                double min_mllk = HUGE_VAL;
                int min_i = 0;
                for (int i = 0; i < m_k; ++i) {
                    mp_sa->mp_pp->mp_stim->set_stim(stims[i]);
                    double mllk = mp_sa->mp_pp->calc_mllk(ss);
                    if (mllk < min_mllk) {
                        min_mllk = mllk;
                        min_i = i;
                    }
                }
                clusters[min_i].push_back(ss);
                *indit = min_i;
                ++indit;
            }

            // optimize each cluster with sa
            double mllk = 0;
            for (auto& c : clusters) {
                auto res = mp_sa->run(c.second, stims[c.first]);
                stims[c.first] = std::get<1>(res);
                mllk += std::get<0>(res);
            }

            // stop em if mllk do not decrease
            if (std::abs(mllk - mllk_old) < 1e-5 * std::abs(mllk + 1e-5)) {
                mllk_old = mllk;
                break;
            }

            mllk_old = mllk;
            step ++;

            if (step > 25) break;

            /*
            std::cout << step << std::endl;
            std::cout << mllk << std::endl;
            for (auto i : indexes) {
                std::cout << i;
            }
            std::cout << std::endl;
            */
        }

        /*
        std::cout << mllk_old << "\t";
        for (auto i : indexes) {
            std::cout << i;
        }
        std::cout << std::endl;
        */

        return Result{mllk_old, indexes, stims, 0, 0};
    }

    // FIXME em trace with method 2
    Result run(int method, bool estimate = true) {
        // to calibrate time, run this first
        //random_init();
        // pick initials
        std::vector<std::vector<int>> stims;
        auto start = std::chrono::system_clock::now();
        if (method == 0)
            stims = random_init();
            //stims = ldci(estimate, true).stim;
        else if (method == 1)
            stims = kmpp();
        else if (method == 2)
            stims = ldci(estimate).stim;
        auto end = std::chrono::system_clock::now();

        std::chrono::duration<double> elapsed = end - start;
        double init_time = elapsed.count();

        start = std::chrono::system_clock::now();
        auto res = run(stims);
        end = std::chrono::system_clock::now();

        elapsed = end - start;
        double em_time = elapsed.count();

        res.init_time = init_time;
        res.em_time = em_time;

        return res;
    }

    Result run(int method, int repnum) {
        std::vector<Result> results(repnum);
        results[0] = run(method);
        std::generate(results.begin()+1, results.end(), [&]() {
            return run(method, false);
        });

        double init_time = 0, em_time = 0;
        for (auto& res : results) {
            init_time += res.init_time;
            em_time += res.em_time;
        }

        auto result = *std::min_element(results.begin(), results.end(), 
                [&](Result a, Result b) { 
                    return a.mllk < b.mllk; 
                });
        result.init_time = init_time;
        result.em_time = em_time;
        return result;
    }


    Result optimal_bayes(const std::vector<std::vector<int>>& stims_in) {
        auto stims = stims_in;
        std::vector<int> indexes(m_ss.size());

        auto indit = indexes.begin();
        double total_mllk = 0;
        for (auto& ss : m_ss) {
            double min_mllk = HUGE_VAL;
            int min_i = 0;
            for (int i = 0; i < m_k; ++i) {
                mp_sa->mp_pp->mp_stim->set_stim(stims[i]);
                double mllk = mp_sa->mp_pp->calc_mllk(ss);
                if (mllk < min_mllk) {
                    min_mllk = mllk;
                    min_i = i;
                }
            }
            total_mllk += min_mllk;
            *indit = min_i;
            ++indit;
        }
        /*
        std::cout << total_mllk << "\t";
        for (auto i : indexes) {
            std::cout << i;
        }
        std::cout << std::endl;
        */

        return Result{total_mllk, indexes, stims, 0, 0};
        //return run(stims_in);
    }


private:
    std::vector<std::vector<int>> m_ss;
    int m_k;            // number of clusters
    SimulatedAnnealing* mp_sa;

};


} // namespace mass


#endif // DISCRETEDECODE_H
